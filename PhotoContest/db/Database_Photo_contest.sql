create or replace table categories
(
    category_id   int auto_increment
        primary key,
    category_name varchar(50) not null
);

create or replace table places
(
    place_id   int auto_increment
        primary key,
    place_name varchar(20) not null
);

create or replace table ranks
(
    rank_id   int auto_increment
        primary key,
    rank_name varchar(20) not null,
    min_score int         not null,
    max_score int         not null
);

create or replace table roles
(
    role_id   int auto_increment
        primary key,
    role_name varchar(50) not null
);

create or replace table users
(
    user_id              int auto_increment
        primary key,
    first_name           varchar(50)                            not null,
    last_name            varchar(50)                            not null,
    email                varchar(100)                           not null,
    username             varchar(50)                            not null,
    password             varchar(128)                           not null,
    role_id              int                                    not null,
    created_at           timestamp  default current_timestamp() null,
    rank_id              int                                    not null,
    total_score          int                                    null,
    reset_password_token varchar(50)                            null,
    isInactive           tinyint(1) default 0                   not null,
    constraint users_pk2
        unique (email),
    constraint users_pk3
        unique (username),
    constraint users_ranks_rank_id_fk
        foreign key (rank_id) references ranks (rank_id),
    constraint users_roles_role_id_fk
        foreign key (role_id) references roles (role_id)
);

create or replace table contests
(
    contest_id       int auto_increment
        primary key,
    title            varchar(50)                            not null,
    category_id      int                                    not null,
    phase_one_start  timestamp  default current_timestamp() null,
    phase_one_end    datetime                               not null,
    phase_two_start  datetime                               not null,
    phase_two_end    datetime                               not null,
    cover_photo_name varchar(1000)                          null,
    user_id          int                                    not null,
    created_at       timestamp  default current_timestamp() null,
    is_invitational  tinyint(1) default 0                   not null,
    constraint contests_pk
        unique (title),
    constraint contests_categories_category_id_fk
        foreign key (category_id) references categories (category_id),
    constraint contests_phases_phase_id_fk
        foreign key (user_id) references users (user_id)
);

create or replace table contest_photos
(
    contest_photo_id int auto_increment
        primary key,
    user_id          int                                   not null,
    contest_id       int                                   not null,
    title            varchar(50)                           not null,
    story            varchar(8150)                         not null,
    created_at       timestamp default current_timestamp() null,
    photo_url        varchar(320)                          not null,
    constraint contest_photos_contests_contest_id_fk
        foreign key (contest_id) references contests (contest_id),
    constraint contest_photos_users_user_id_fk
        foreign key (user_id) references users (user_id)
);

create or replace table invited_users
(
    invited_users_id int auto_increment
        primary key,
    user_id          int not null,
    contest_id       int not null,
    constraint invited_users_contests_contest_id_fk
        foreign key (contest_id) references contests (contest_id),
    constraint invited_users_users_user_id_fk
        foreign key (user_id) references users (user_id)
);

create or replace table jury_members
(
    jury_member_id int auto_increment
        primary key,
    user_id        int not null,
    contest_id     int not null,
    constraint jury_members_contests_contest_id_fk
        foreign key (contest_id) references contests (contest_id),
    constraint jury_members_users_user_id_fk
        foreign key (user_id) references users (user_id)
);

create or replace table photo_reviews
(
    photo_review_id   int auto_increment
        primary key,
    contest_photo_id  int           not null,
    user_id           int           not null,
    comment           varchar(3000) null,
    is_wrong_category tinyint(1)    not null,
    juro_points       int           not null,
    contest_id        int           not null,
    constraint photo_reviews_contest_photos_contest_photo_id_fk
        foreign key (contest_photo_id) references contest_photos (contest_photo_id),
    constraint photo_reviews_contests_contest_id_fk
        foreign key (contest_id) references contests (contest_id),
    constraint photo_reviews_jury_members_jury_member_id_fk
        foreign key (user_id) references users (user_id)
);

create or replace table winner_photos
(
    winner_photo_id  int auto_increment
        primary key,
    contest_photo_id int not null,
    place_id         int not null,
    points_winners   int null,
    constraint winner_photos_contest_photos_contest_photo_id_fk
        foreign key (contest_photo_id) references contest_photos (contest_photo_id),
    constraint winner_photos_places_place_id_fk
        foreign key (place_id) references places (place_id)
);

