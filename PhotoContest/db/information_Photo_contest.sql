-- --------------------------------------------------------
-- Host:                         127.0.0.1
-- Server version:               10.5.18-MariaDB - mariadb.org binary distribution
-- Server OS:                    Win64
-- HeidiSQL Version:             11.3.0.6295
-- --------------------------------------------------------

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET NAMES utf8 */;
/*!50503 SET NAMES utf8mb4 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

-- Dumping data for table photo_contest.categories: ~13 rows (approximately)
/*!40000 ALTER TABLE `categories` DISABLE KEYS */;
INSERT INTO `categories` (`category_id`, `category_name`) VALUES
	(1, 'Mountains'),
	(2, 'Forests'),
	(3, 'Sea'),
	(4, 'Cities'),
	(5, 'Villages'),
	(6, 'Flowers'),
	(7, 'Holidays'),
	(8, 'People'),
	(9, 'Treasures'),
	(10, 'Food'),
	(11, 'Animal'),
	(20, 'Seasons'),
	(21, 'Weather');
/*!40000 ALTER TABLE `categories` ENABLE KEYS */;

-- Dumping data for table photo_contest.contests: ~12 rows (approximately)
/*!40000 ALTER TABLE `contests` DISABLE KEYS */;
INSERT INTO `contests` (`contest_id`, `title`, `category_id`, `phase_one_start`, `phase_one_end`, `phase_two_start`, `phase_two_end`, `cover_photo_name`, `user_id`, `created_at`, `is_invitational`) VALUES
	(18, 'Mountains', 5, '2023-04-25 13:55:58', '2023-04-27 18:41:00', '2023-04-27 16:41:00', '2023-04-27 19:35:00', 'https://cdn.pixabay.com/photo/2017/07/24/12/43/schrecksee-2534484__340.jpg', 16, '2023-04-27 13:55:58', 0),
	(19, 'Beaches', 3, '2023-04-25 13:55:58', '2023-04-28 10:00:00', '2023-04-28 10:00:00', '2023-04-28 14:36:00', 'https://velstana.com/wp-content/uploads/home-black-sea-coast.jpg', 16, '2023-04-27 15:12:59', 0),
	(20, 'Beautiful flowers in Spring', 6, '2023-04-25 13:55:58', '2023-04-28 10:00:00', '2023-04-28 10:00:00', '2023-04-28 14:36:00', 'https://cdn.pixabay.com/photo/2016/07/11/20/37/marguerite-1510602__340.jpg', 16, '2023-04-27 15:15:43', 0),
	(21, 'Old houses', 5, '2023-04-25 13:55:58', '2023-04-28 10:00:00', '2023-04-28 10:00:00', '2023-04-28 14:36:00', 'https://www.andrey-andreev.com/wp-content/uploads/2013/12/IMGP284720182.jpg', 16, '2023-04-27 15:18:20', 0),
	(22, 'The magic of Surva', 7, '2023-04-25 13:55:58', '2023-04-28 10:00:00', '2023-04-28 10:00:00', '2023-04-28 14:36:00', 'https://cdn1.matadornetwork.com/blogs/1/2020/12/Surva-Festival-6.jpg', 2, '2023-04-27 15:22:04', 0),
	(23, 'The beauty of Autumn', 20, '2023-04-25 13:55:58', '2023-04-28 10:00:00', '2023-04-28 10:00:00', '2023-04-28 14:36:00', 'https://cdn.pixabay.com/photo/2019/10/30/09/30/vignoble-4589078__340.jpg', 2, '2023-04-27 15:43:14', 1),
	(24, 'View from my window', 4, '2023-04-25 13:55:58', '2023-04-28 10:00:00', '2023-04-28 10:00:00', '2023-04-28 14:36:00', 'https://thumbs.dreamstime.com/z/sofia-bulgaria-august-amazing-aerial-view-national-palace-culture-city-capital-163396806.jpg', 2, '2023-04-27 15:47:11', 0),
	(25, 'Forest in winter', 2, '2023-04-25 13:55:58', '2023-04-28 10:00:00', '2023-04-28 10:00:00', '2023-04-28 14:36:00', 'https://images6.alphacoders.com/676/676694.jpg', 3, '2023-04-27 16:10:56', 0),
	(26, 'Easter tradition', 7, '2023-04-28 14:36:25', '2023-05-03 13:35:00', '2023-05-03 13:35:00', '2023-05-07 18:35:00', 'https://assets3.thrillist.com/v1/image/2962411/1200x600/scale.png', 4, '2023-04-28 14:36:25', 0),
	(27, 'Show your town', 4, '2023-04-28 14:37:49', '2023-05-06 14:37:00', '2023-05-06 14:37:00', '2023-05-06 20:37:00', 'https://www.roadaffair.com/wp-content/uploads/2018/07/walkway-center-ruse-bulgaria-shutterstock_676801681.jpg', 4, '2023-04-28 14:37:49', 0),
	(28, 'Rainy', 21, '2023-04-28 14:40:56', '2023-05-05 09:39:00', '2023-05-05 09:39:00', '2023-05-05 19:39:00', 'https://images.contentstack.io/v3/assets/bltba617d00249585dc/blt403c8f7d347e0a4b/6095b2044adba359327cc1ff/1-rainyday-herocard-1440x1080.jpg', 4, '2023-04-28 14:40:56', 0),
	(29, 'Animals in mountain', 11, '2023-04-28 14:44:02', '2023-05-05 09:02:00', '2023-05-05 09:02:00', '2023-05-05 21:42:00', 'https://upload.wikimedia.org/wikipedia/commons/thumb/0/0e/Bos_grunniens_at_Letdar_on_Annapurna_Circuit.jpg/1200px-Bos_grunniens_at_Letdar_on_Annapurna_Circuit.jpg', 4, '2023-04-28 14:44:02', 0);
/*!40000 ALTER TABLE `contests` ENABLE KEYS */;

-- Dumping data for table photo_contest.contest_photos: ~49 rows (approximately)
/*!40000 ALTER TABLE `contest_photos` DISABLE KEYS */;
INSERT INTO `contest_photos` (`contest_photo_id`, `user_id`, `contest_id`, `title`, `story`, `created_at`, `photo_url`) VALUES
	(12, 15, 18, 'Rila in summer', 'Once upon a time, in the heart of the majestic mountains, there was a small village called Pinecrest. It was a beautiful place surrounded by tall trees, crystal clear rivers, and snow-capped peaks. The villagers were simple folk who led a peaceful life and were content with what they had.', '2023-04-27 14:29:32', 'src/main/resources/static/uploads/2023c1cc-9c6c-4dbf-b984-a0422eb50211flowers-276014_960_720.jpg'),
	(13, 5, 18, 'Great view', 'Once upon a time, there was a young man named Jack who had always been fascinated by the mountains. He dreamed of climbing the highest peaks and exploring the rugged terrain. Despite growing up in a flat, urban area, Jack\'s passion for the mountains never waned.', '2023-04-27 18:33:19', 'src/main/resources/static/uploads/e5445b79-b9c3-4c72-b49c-75dc47b6d78cpirin-guided-trekking-tours.jpg'),
	(14, 6, 18, 'Stunning picture', 'Jack\'s love for the mountains never waned, and he spent the rest of his life exploring the world\'s most rugged and remote landscapes. His adventures inspired countless others to follow in his footsteps and discover the magic of the mountains for themselves.', '2023-04-27 18:34:07', 'src/main/resources/static/uploads/eaefd6f9-c75b-433e-9b70-1197cf51958eBulgaria-Lake-Bliznaka-Rila-mountains.jpg'),
	(15, 7, 18, 'Mountains flowers', 'One day, a young woman named Lily stumbled upon the field while hiking through the mountains. She was immediately entranced by the beauty of the flowers and decided to pick a few to take back with her to her village. But as she plucked them from the ground, she realized that the flowers were wilting in her hand.', '2023-04-27 18:35:46', 'src/main/resources/static/uploads/228e71ee-43fb-42ff-af7e-23e33c738285Bulgaria-Rila-mountains-crocus.jpg'),
	(18, 9, 18, 'Pirin`s peak', 'As he reached higher altitudes, the air grew thinner, and the climb became more challenging. But Jack was determined to push through the difficulties and reach the top. Finally, after many hours of climbing, he reached the summit, and the view from the top was breathtaking.', '2023-04-27 18:38:46', 'src/main/resources/static/uploads/9c77d515-1792-435b-870c-272582f5a9cabalkan-mountains-hiking-and-walking-tours-bulgaria.jpg'),
	(19, 10, 18, 'People in mountain', 'From that moment on, Jack was hooked. He spent years exploring mountain ranges all over the world, challenging himself to climb higher peaks and explore more difficult terrain. He learned to respect the mountains and the dangers they posed, but he also discovered a sense of peace and fulfillment in the beauty and solitude of the high altitudes.', '2023-04-27 18:40:21', 'src/main/resources/static/uploads/24eeeedf-fb15-4571-b2b4-b78153a20784musala-hiking-1.jpg'),
	(20, 11, 25, 'Winter in Rila', 'She spread awareness about the importance of preserving the delicate balance of the mountain ecosystem, and the flowers became a symbol of hope and resilience for the community.', '2023-04-27 19:07:23', 'src/main/resources/static/uploads/ab7a7203-a788-4c7e-afda-bb1853b940d2bulgaria-snow-shoe trek.jpg'),
	(22, 17, 25, 'Winter sun', 'As the snow began to subside, the town emerged from the storm, stronger and more united than ever before. The streets were now clear, and the sun shone bright on the glistening snow. The children once again returned to their outdoor activities, this time with even more enthusiasm than before, as they had a newfound appreciation for the beauty and power of winter.', '2023-04-27 22:43:47', 'src/main/resources/static/uploads/d10921ad-31fa-40e7-9671-bc230ae18e88HD-wallpaper-winter-forest-trees-snow-forest-path-sunshine-winter.jpg'),
	(23, 17, 24, 'View in Plovdiv', 'From that day forward, the people of the town looked forward to winter not just for the fun it brought but also for the opportunity it provided to come together and show the strength of their community.', '2023-04-27 22:45:45', 'src/main/resources/static/uploads/d270db41-1508-4eac-95d0-3396cecf6815photo-1514580597161-eb1c0b1a7971.jpg'),
	(24, 17, 22, 'Tradition in action', 'Surva is a traditional Bulgarian folk festival celebrated in January, which is also known as the "International Festival of the Masquerade Games". It is one of the oldest Bulgarian customs, dating back to pagan times, and it is believed to bring good luck and prosperity for the coming year.', '2023-04-27 22:50:41', 'src/main/resources/static/uploads/a0ce80de-6c97-4309-833e-24c239694d28sruvabg.jpg'),
	(25, 17, 21, 'House in Koprivshtitsa', 'Old houses in Bulgaria have a unique architectural style that reflects the country\'s long and diverse history. Many of these houses were built during the Ottoman Empire, which ruled over Bulgaria for nearly five centuries until the late 19th century. Other styles, such as the Revival Period, also had a significant impact on the design of old Bulgarian houses.', '2023-04-27 22:53:07', 'src/main/resources/static/uploads/f1eb2bc4-ebdf-43d3-985b-5f07421a28cbEmily-Lush-Plovdiv-19.jpg'),
	(27, 17, 20, 'How wonderful', 'Once upon a time, in a beautiful meadow surrounded by rolling hills, there were fields of flowers as far as the eye could see. There were roses in every shade of pink and red, daisies with their cheerful white petals, and sunflowers with their bright yellow faces always turned towards the sun.', '2023-04-27 22:55:55', 'src/main/resources/static/uploads/9d67f859-df5e-495c-98ca-0354863c16942tljlk1qyjg8dyjk.jpg'),
	(28, 17, 19, 'Summer dream', 'There was a young girl named Marina who lived by the sea. She loved the sound of the waves and the smell of the salty air. She spent her days walking along the shore, collecting seashells and watching the sea creatures swim by.', '2023-04-27 22:57:55', 'src/main/resources/static/uploads/5ac168ca-95d4-4c24-a1c0-caf6e4feeaf8visit-bulgaria-seaside-cities.jpg'),
	(29, 5, 23, 'Autumn in the forest', 'As the days grew shorter and the nights grew longer, the villagers came together to celebrate the harvest. They gathered in the town square, roasting marshmallows over a crackling bonfire and sharing stories and laughter.', '2023-04-27 22:59:50', 'src/main/resources/static/uploads/d96b8156-2525-462a-a1a1-c954a6903e40338596.jpg'),
	(30, 5, 25, 'Sun in winter', 'Despite the cold, the people of the town were excited about the arrival of winter. They knew that with the snow came the opportunity for all kinds of fun activities. Children bundled up in their warmest coats and mittens, eagerly headed outside to build snowmen, go sledding, and have snowball fights.', '2023-04-27 23:01:12', 'src/main/resources/static/uploads/67606ae9-ef8d-4642-a1ad-62405112fa20High_resolution_wallpaper_background_ID_77701306963.jpg'),
	(32, 5, 24, 'View in the middle of the town', 'The air was cool and crisp, and the ground was covered in a thick layer of leaves that crunched and rustled underfoot. The smell of woodsmoke wafted through the air, and the sound of migrating geese honking could be heard in the distance.', '2023-04-27 23:02:49', 'src/main/resources/static/uploads/8d781451-ea9a-4734-8d9b-a7d003de32f7View_out_of_window.jpg'),
	(33, 5, 22, 'Festival of the people', 'During the Surva festival, people dress up in costumes and masks made from animal skins, wool, and feathers. They roam the streets, singing and dancing to the beat of traditional Bulgarian music. The costumes are often adorned with bells, and the sound of their ringing is believed to ward off evil spirits.', '2023-04-27 23:04:24', 'src/main/resources/static/uploads/b33a40e8-deb0-4a99-a827-debdf389da1c01_kurva_festival_bulgaria.adapt.676.1-21483-1140x0.jpg'),
	(34, 5, 21, 'The secret of the house', 'Old Bulgarian houses also had thick walls made of stone or mud brick, which provided excellent insulation against both the cold winters and the scorching summers. The walls were often decorated with intricate carvings or painted in bright, vibrant colors.', '2023-04-27 23:05:16', 'src/main/resources/static/uploads/9535fab7-e0a8-46d9-8a0d-28062291434a78fd4d2396e0d2e0227cd62468136639.jpg'),
	(35, 5, 20, 'When the nature is beautiful', 'Every day, the flowers would wake up with the rising sun and bask in its warm glow. They would stretch their petals and soak up the morning dew, which made their colors even more vibrant.', '2023-04-27 23:05:49', 'src/main/resources/static/uploads/5dae0819-0e90-413f-a230-70d125dbd640flowers-3840x2160-4k-hd-wallpaper-green-grass-nature-5189.jpg'),
	(36, 5, 19, 'Summer sun', 'One day, as she was walking along the shore, she saw a creature she had never seen before. It was a beautiful mermaid with long, flowing hair and a shimmering tail. The mermaid sang a hauntingly beautiful song that echoed across the sea.', '2023-04-27 23:06:19', 'src/main/resources/static/uploads/ac534e3a-afa1-406f-8962-e5535ce9cbdaBulgaria_Bolata-Beach_shutterstock_1008683605-1.jpg'),
	(37, 6, 23, 'The cold part of autumn', 'In the village, the people were busy preparing for the season. The farmers worked long hours in the fields, harvesting the last of the summer crops and preparing for the winter ahead. The bakers were hard at work, creating pies and breads flavored with the sweetest apples and the richest pumpkins. The children, meanwhile, were busy jumping in leaf piles and picking apples from the trees.', '2023-04-27 23:07:52', 'src/main/resources/static/uploads/599b39c3-7a73-4dab-9f22-c4f1d62af322wp7408836.jpg'),
	(38, 6, 25, 'White dream', 'One day, as the townsfolk were out enjoying the winter wonderland, a sudden blizzard hit the area. The winds howled, and the snow fell so thick and fast that it was impossible to see more than a few feet in front of them.', '2023-04-27 23:09:12', 'src/main/resources/static/uploads/6ffb623d-e5b5-46d7-9bfe-ed4b8a30c3acwinter-7689873_1280.jpg'),
	(39, 6, 24, 'Cat and window', 'Once upon a time, in the beautiful state of California, there was a young man named Carlos. He lived in a small town in the heart of the Central Valley, surrounded by fields of crops and orchards of fruit trees.', '2023-04-27 23:11:48', 'src/main/resources/static/uploads/5bf5eb48-f167-40d6-8e90-6bc99ad98cc0_112382583_14.jpg'),
	(40, 6, 22, 'Bulgarian holiday', 'The Surva festival is not just a celebration of tradition and culture; it is also a time of unity and community. The festival brings together people of all ages and backgrounds, who come together to celebrate and honor their heritage. It is a time to forget about worries and to enjoy life, to dance, to sing, and to have fun with friends and family.', '2023-04-27 23:12:51', 'src/main/resources/static/uploads/3a54fc84-a4a8-4e24-b075-c22310bdc500resize_270227798_258039663086569_4046740294772296788_n.jpg'),
	(41, 6, 21, 'House in Triyavna', 'One of the most striking features of old Bulgarian houses is their beautiful courtyards, which were often surrounded by high walls or hedges to provide privacy. The courtyards were typically filled with fruit trees, grape vines, and colorful flowers, and they were a popular gathering place for families and friends.', '2023-04-27 23:13:35', 'src/main/resources/static/uploads/24e65bda-6ef2-43de-ba8e-33ba0f757138unsplash-image-ZKusmVu_98k.jpg'),
	(42, 6, 20, 'Flowers and the sun', 'One day, a little girl wandered into the meadow. She was in search of a special flower to give to her mother, who was sick and needed cheering up. As she walked through the fields, the flowers noticed her and began to whisper to each other.', '2023-04-27 23:15:04', 'src/main/resources/static/uploads/21c8dccc-c57d-453c-a0db-3e913797a3a0HD-wallpaper-beautiful-garden-gate-garden-flowers-beautiful.jpg'),
	(43, 6, 19, 'Sea wants me', 'As they returned to the surface, Marina knew that she had experienced something truly magical. She thanked the mermaid and promised to visit her again soon. From that day on, Marina spent every chance she could exploring the wonders of the sea and the creatures that called it home.', '2023-04-27 23:16:36', 'src/main/resources/static/uploads/72ff9e5c-f95a-4f45-8eca-cedf5dfeb6c6coast-social.jpg'),
	(44, 7, 23, 'Autumn love', 'And when it finally did, the skies cleared, and the sun shone once again. The village emerged from the storm, more beautiful than ever, with the leaves shimmering like gold in the sunshine.', '2023-04-27 23:20:21', 'src/main/resources/static/uploads/f4598d3c-989e-466e-ab88-6b14870a2e26header-autumn-phoenix-park-dublin.jpg'),
	(45, 7, 25, 'Great winter', 'From that day forward, the people of the town looked forward to winter not just for the fun it brought but also for the opportunity it provided to come together and show the strength of their community.', '2023-04-27 23:22:08', 'src/main/resources/static/uploads/51d61a43-0e17-45fb-87fb-01225ea89eabHD-wallpaper-forest-snow-winter-lake-scenic-trees-landscape.jpg'),
	(46, 7, 24, 'Interesting view', 'But the villagers were not afraid. They knew that the storm would pass, just as the autumn season would give way to winter. They huddled together in their homes, sharing warmth and companionship, and waited for the storm to pass.', '2023-04-27 23:23:02', 'src/main/resources/static/uploads/618b4e17-5535-41f0-8e94-707cbb2747990x0.jpg'),
	(47, 7, 22, 'Scary mask', 'The Surva festival is an important part of Bulgarian culture, and it is a unique and unforgettable experience for anyone who has the opportunity to witness it.', '2023-04-27 23:24:10', 'src/main/resources/static/uploads/c9d42d64-0acf-4da3-b958-02e79241e6a9768x432.jpg'),
	(48, 7, 21, 'Old time', 'Today, many old houses in Bulgaria have been preserved as historic landmarks, museums, or guesthouses. They offer a glimpse into the country\'s rich cultural heritage and provide a unique and memorable experience for visitors.', '2023-04-27 23:25:50', 'src/main/resources/static/uploads/46b6b2c6-5707-4d5e-9534-6282a7807bd3d438a86a474f2098c2fa6f324b2f2a83.jpg'),
	(49, 7, 20, 'Beauty everywhere', 'Once upon a time, there was a small town nestled in the countryside, surrounded by rolling hills and meadows of wildflowers. As winter faded away, the townspeople eagerly awaited the arrival of spring.', '2023-04-27 23:27:16', 'src/main/resources/static/uploads/d8112890-e0d6-475e-8572-b1450d09b4a7IMAGE_7333363_4000_0.jpg'),
	(50, 7, 19, 'Stunning rocks', 'From that day on, the town continued to thrive, filled with the beauty and energy of spring. The flowers bloomed, the bees buzzed, and the fields were alive with the sounds of nature. And the townspeople knew that every year, when spring arrived, they would celebrate and embrace the magic of new beginnings.', '2023-04-27 23:29:54', 'src/main/resources/static/uploads/7b7df079-14bd-42e3-9de0-caba9b39ef3aIMGP5702.jpg'),
	(51, 5, 29, 'Bear in Pirin', 'Once upon a time, in a dense forest, there lived a mighty and fierce bear named Bruno. Bruno was the king of the forest, and all the other animals lived in fear of him. He was massive in size, with sharp teeth and claws that could tear through anything that stood in his way.', '2023-04-28 14:57:56', 'src/main/resources/static/uploads/306f07cb-8f05-431a-aa94-708244386e05original.jpg'),
	(52, 5, 28, 'Rainy flower', 'Bruno, seeing the hikers\' plight, sprang into action. He approached the hikers, and with a series of growls and grunts, he managed to convey to them that he would help them down the cliff.', '2023-04-28 14:58:26', 'src/main/resources/static/uploads/09aaa213-b02c-4459-b52e-e26a3c511f1c1-rain-wallpaper-drops.jpg'),
	(53, 5, 27, 'Sunny Sofia', 'Once upon a time, in a small village nestled in the mountains, there lived a young girl named Sofia. Sofia was a kind and curious girl, who loved exploring the natural world around her. She had a passion for plants and animals and spent most of her days wandering through the forest and fields, observing and learning about the creatures and plants that lived there.', '2023-04-28 14:59:06', 'src/main/resources/static/uploads/c2667dcb-9d9f-4fa5-957e-569cf9fc3785one-day-in-sofia-e1637405547150.jpg'),
	(54, 5, 26, 'Beautiful eggs', 'Sofia learned a lot from the old man and realized that helping others and being kind was not only important but also rewarding. From that day on, Sofia made it her mission to help those in need and to spread kindness wherever she went.', '2023-04-28 14:59:36', 'src/main/resources/static/uploads/249d8701-aff5-42e1-89eb-4cb38abf1e91easter-basket-ideas-ruffles-1647370039.jpeg'),
	(55, 10, 29, 'Wild nature', 'Once upon a time, there was a mighty mountain named Mount Everest. The mountain towered high above the surrounding landscape, with snow-capped peaks that reached up into the clouds. It was a majestic sight, and people from all over the world came to climb it, seeking adventure and a chance to conquer the tallest mountain in the world.', '2023-04-28 15:01:13', 'src/main/resources/static/uploads/5a596e4e-5c4a-4e08-a01c-b5e34192b262Eagle-2-1.jpg'),
	(56, 10, 27, 'Beauty of Sozopol', 'Many adventurers had tried to find the cave and the crystal, but none had ever succeeded. It was said that the mountain was fiercely protective of its secret and would do anything to prevent anyone from finding it.', '2023-04-28 15:25:34', 'src/main/resources/static/uploads/8f84d573-11bc-4965-9479-670608c96db214876888081573365142_411793332_1746406858.jpg'),
	(57, 10, 26, 'Holiday mood', 'But as Aiden was about to leave the cave, he heard a voice that shook the ground beneath his feet. It was the voice of the mountain, and it was angry. The mountain was furious that someone had found its secret and was threatening to take the crystal away.', '2023-04-28 15:26:05', 'src/main/resources/static/uploads/78d45bf0-ecef-4b6c-82bd-6b2af3834e8f8931-istockgetty-images-plusasife.jpg'),
	(58, 8, 29, 'Mountain life', 'Aiden knew he had to act quickly. He knew that the mountain was not just an obstacle to overcome but a living being with feelings and emotions. He spoke to the mountain, reassuring it that he meant no harm and that he only wanted to learn from the crystal.', '2023-04-28 15:29:33', 'src/main/resources/static/uploads/b454d260-3b06-4479-ac6c-6d5fa6c9d25dBighornRams.jpg'),
	(59, 8, 28, 'Rainy life', 'from a clear blue sky, and the ground was dry and cracked. The crops were withering away, and the villagers were starting to lose hope.', '2023-04-28 15:30:23', 'src/main/resources/static/uploads/1bbda0b6-ff88-47fa-b212-7189da6ec3753a5e4be3a1574d2484784a508c3250a6.jpg'),
	(60, 8, 26, 'Easter spirit', 'The town was adorned with brightly colored ribbons, flowers, and Easter eggs. The streets were filled with the sounds of children laughing and playing, and the delicious aroma of Easter treats wafted through the air.', '2023-04-28 15:31:25', 'src/main/resources/static/uploads/317a2c05-e222-4b0b-be04-42ec91bb4ca2kids-easter-egg-games-1695686_hero-93183ae0831845ea8bee28290e59222d.jpg'),
	(61, 11, 29, 'Funny nature', 'As the hikers tried to find their way out of the woods, Bruno followed them, keeping a watchful eye on them. Eventually, the hikers stumbled upon a steep cliff that they couldn\'t climb down. They were stuck, and they had no idea how to get back to their campsite.', '2023-04-28 15:32:26', 'src/main/resources/static/uploads/6222ce86-9869-46b9-a6e2-e950070787b6marmot-1.jpg'),
	(62, 11, 28, 'Rainy day', 'But as the days went by, the rain kept falling, harder and harder. The gentle drops turned into a raging downpour, and the villagers began to worry. The streams and rivers were swelling, and the ground was becoming saturated. The rain showed no signs of stopping.', '2023-04-28 15:33:04', 'src/main/resources/static/uploads/c97f9754-ecce-42fd-acb7-1eea597d3a89desktop-wallpaper-early-spring-rain-flowers-springtime-rains.jpg'),
	(63, 11, 27, 'Plovdiv`s street', 'The villagers started to panic, fearing that the rain would destroy their homes and crops. But then, an old woman spoke up. She reminded the villagers that rain was a gift from nature, and that they needed to respect and honor it. She told them that the rain was not their enemy, but their friend, and that they needed to find a way to work with it.', '2023-04-28 15:33:45', 'src/main/resources/static/uploads/87e64ba3-168d-443d-a48d-d9345ea26d4511.jpg'),
	(64, 11, 26, 'Spring is here', 'As the sun began to set, the townspeople gathered around a large bonfire, with the fire\'s warm glow casting dancing shadows on their faces. A group of children sang songs of praise, and a local priest led the people in a prayer of thanksgiving for the blessings of life, love, and faith.', '2023-04-28 15:34:23', 'src/main/resources/static/uploads/a6d27a4e-c8ea-4178-bd11-ed643b51c5afB7WmyEB7YpQMk35VPT8EP3-1200-80.jpg'),
	(65, 17, 26, 'Easter island in Spring', 'Island tradition claims that around 1680, after peacefully coexisting for many years, one of the island\'s two main groups, known as the Short-Ears, rebelled against the Long-Ears, burning many of them to death on a pyre constructed along an ancient ditch at Poike, on the island\'s far northeastern coast.', '2023-05-03 14:19:05', 'src/main/resources/static/uploads/6cf7cf6f-0fea-4dd8-82a6-2cd3d414c0fceaster-island-heads.jpg');
/*!40000 ALTER TABLE `contest_photos` ENABLE KEYS */;

-- Dumping data for table photo_contest.invited_users: ~3 rows (approximately)
/*!40000 ALTER TABLE `invited_users` DISABLE KEYS */;
INSERT INTO `invited_users` (`invited_users_id`, `user_id`, `contest_id`) VALUES
	(15, 5, 23),
	(16, 6, 23),
	(17, 7, 23);
/*!40000 ALTER TABLE `invited_users` ENABLE KEYS */;

-- Dumping data for table photo_contest.jury_members: ~1 rows (approximately)
/*!40000 ALTER TABLE `jury_members` DISABLE KEYS */;
INSERT INTO `jury_members` (`jury_member_id`, `user_id`, `contest_id`) VALUES
	(1, 18, 26);
/*!40000 ALTER TABLE `jury_members` ENABLE KEYS */;

-- Dumping data for table photo_contest.photo_reviews: ~130 rows (approximately)
/*!40000 ALTER TABLE `photo_reviews` DISABLE KEYS */;
INSERT INTO `photo_reviews` (`photo_review_id`, `contest_photo_id`, `user_id`, `comment`, `is_wrong_category`, `juro_points`, `contest_id`) VALUES
	(25, 12, 16, 'This photo is truly stunning. The composition is perfect, with the subject of the photo perfectly framed and in focus.', 0, 10, 18),
	(26, 12, 2, 'The lighting is soft and natural, casting a warm glow over the scene. The colors are vibrant and balanced, creating a beautiful contrast between the blue sky and the green trees.', 0, 8, 18),
	(27, 12, 3, 'What really stands out about this photo is the way it captures a sense of tranquility and peace.', 0, 7, 18),
	(28, 13, 2, 'The subject of the photo, a solitary figure sitting on a bench, appears lost in thought, surrounded by the beauty of nature. It\'s a powerful image that speaks to the human need for connection with the natural world.', 0, 8, 18),
	(29, 14, 2, 'Overall, this photo is a true work of art, capturing a moment of beauty and stillness that is both captivating and inspiring.', 0, 8, 18),
	(30, 15, 2, 'It\'s the kind of photo that draws you in and invites you to stay a while, and it\'s a testament to the photographer\'s skill and vision.', 0, 3, 18),
	(31, 18, 2, 'This photo is simply breathtaking. The composition is expertly crafted, with the subject of the photo perfectly positioned and framed within the scene. ', 0, 7, 18),
	(32, 19, 2, 'The lighting is exquisite, casting a soft, warm glow over the entire image. The colors are vibrant and balanced, creating a harmonious and visually pleasing effect.', 0, 10, 18),
	(33, 13, 3, 'What sets this photo apart is its emotional impact. It captures a moment of pure joy and wonder, as the subject of the photo reaches out to touch a rainbow.', 0, 8, 18),
	(34, 14, 3, 'The rainbow itself is a symbol of hope and optimism, and the image as a whole is a testament to the beauty and power of the natural world.', 0, 8, 18),
	(35, 15, 3, 'Overall, this photo is a true masterpiece, evoking a sense of awe and wonder that is both captivating and inspiring. ', 0, 10, 18),
	(36, 13, 16, 'Default score', 0, 3, 18),
	(37, 14, 16, 'Default score', 0, 3, 18),
	(38, 15, 16, 'Default score', 0, 3, 18),
	(39, 18, 16, 'Default score', 0, 3, 18),
	(40, 19, 16, 'Default score', 0, 3, 18),
	(41, 18, 3, 'Default score', 0, 3, 18),
	(42, 19, 3, 'Default score', 0, 3, 18),
	(43, 20, 16, 'I really enjoyed this photo of winter.', 0, 8, 25),
	(44, 22, 16, 'The snowy landscape is beautifully captured and the contrast between the white snow and the blue sky is striking', 0, 10, 25),
	(45, 30, 16, 'The photographer has also done a great job of framing the image with the trees in the foreground, which adds depth to the picture.', 0, 4, 25),
	(46, 38, 16, 'One thing that stands out to me is the texture of the snow', 0, 8, 25),
	(47, 45, 16, ' The way it\'s been captured almost makes me feel like I\'m there, feeling the crisp winter air and the crunch of the snow under my feet.', 0, 3, 25),
	(48, 23, 16, 'This photo of the views is absolutely breathtaking. ', 0, 8, 24),
	(49, 32, 16, ' The way the photographer has captured the sweeping landscape and the distant mountains is truly awe-inspiring.', 0, 5, 24),
	(50, 39, 16, 'The sense of scale and grandeur is incredible.', 0, 10, 24),
	(51, 46, 16, 'One thing that really stands out to me is the use of light in the photo. ', 0, 9, 24),
	(52, 29, 16, 'I really love this photo of autumn.', 0, 10, 23),
	(53, 37, 16, 'The warm, golden tones of the leaves and the soft, diffused light create a sense of warmth and coziness that\'s perfect for the season. ', 0, 3, 23),
	(54, 44, 16, 'The photographer has done a great job of capturing the beauty and tranquility of the autumn landscape.', 0, 9, 23),
	(55, 24, 16, 'I\'m really drawn to this photo of the surva ritual. ', 0, 9, 22),
	(56, 33, 16, 'The vibrant colors of the costumes and masks are absolutely stunning and really capture the essence of the tradition. ', 0, 9, 22),
	(57, 40, 16, 'The photographer has also done a great job of capturing the motion and energy of the performers in action.', 0, 3, 22),
	(58, 47, 16, 'The focus on the central figure in the frame is also a great touch, as it draws the viewer\'s eye to the heart of the action.', 0, 8, 22),
	(59, 25, 16, 'I really love this photo of the old houses. The way the photographer has captured the intricate details and textures of the buildings is truly impressive.', 0, 7, 21),
	(60, 34, 16, 'One thing that really stands out to me is the composition of the photo.', 0, 8, 21),
	(61, 41, 16, 'The play of light and shadow also adds depth and dimensionality to the image.', 0, 6, 21),
	(62, 48, 16, 'I also appreciate the use of color in the photo. The muted tones and earthy hues of the buildings and the surrounding foliage create a sense of warmth and nostalgia that\'s perfectly suited to the subject matter.', 0, 5, 21),
	(63, 27, 16, 'elicate beauty and intricate details of the blooms is truly impressive. ', 0, 7, 20),
	(64, 35, 16, 'The soft, muted tones and gentle lighting create a sense of tranquility and serenity that\'s perfect for the subject matter.', 0, 10, 20),
	(65, 42, 16, 'One thing that really stands out to me is the composition of the photo.', 0, 8, 20),
	(66, 49, 16, 'The way the flowers are arranged in the frame and the use of negative space create a sense of balance and harmony. The blurred background also adds depth and dimensionality to the image.', 0, 8, 20),
	(67, 28, 16, 'I absolutely love this photo of the sea. The colors are so vibrant and the composition is just stunning. The photographer has done a great job of capturing the motion of the waves and the way the light reflects off the water.', 0, 9, 19),
	(68, 36, 16, 'One thing that really stands out to me is the sense of scale in the photo. ', 0, 6, 19),
	(69, 43, 16, 'The way the rocks in the foreground are juxtaposed against the vastness of the sea in the background creates a sense of depth and perspective that draws the viewer into the image.', 0, 9, 19),
	(70, 50, 16, 'I also appreciate the use of color in the photo. The bright blue of the sky and the deep blues and greens of the water really create a sense of tranquility and calm.', 0, 7, 19),
	(71, 20, 2, 'The way it\'s been captured almost makes me feel like I\'m there, feeling the crisp winter air and the crunch of the snow under my feet.', 0, 7, 25),
	(72, 22, 2, 'Overall, this is a wonderful photo that perfectly captures the beauty of winter. Well done!', 0, 10, 25),
	(73, 30, 2, 'The way it\'s been captured almost makes me feel like I\'m there, feeling the crisp winter air and the crunch of the snow under my feet.', 0, 7, 25),
	(74, 38, 2, 'The photographer has also done a great job of framing the image with the trees in the foreground, which adds depth to the picture.', 0, 4, 25),
	(75, 45, 2, 'I really enjoyed this photo of winter. The snowy landscape is beautifully captured and the contrast between the white snow and the blue sky is striking.', 0, 4, 25),
	(76, 23, 2, 'The sense of scale and grandeur is incredible.', 0, 8, 24),
	(77, 32, 2, 'One thing that really stands out to me is the use of light in the photo. ', 0, 9, 24),
	(78, 39, 2, 'It almost feels like I\'m there, basking in the glow of the sun and taking in the stunning views.', 0, 9, 24),
	(79, 46, 2, 'The composition of the photo is also really interesting. The way the foreground leads the eye to the distant mountains creates a sense of depth and perspective that adds to the overall impact of the image.', 0, 9, 24),
	(80, 29, 2, 'The photographer has done an excellent job of creating a visually stunning image that evokes a sense of peace and tranquility.', 0, 8, 23),
	(81, 37, 2, 'I also appreciate the use of color in the photo. The warm, golden hues of the leaves and the cool blues of the sky complement each other perfectly and create a sense of balance and harmony.', 0, 8, 23),
	(82, 44, 2, ' The way the trees frame the image and create a sense of depth is really effective, and the use of negative space adds to the overall impact of the image. ', 0, 6, 23),
	(83, 24, 2, 'The way the performers are arranged in the frame creates a sense of movement and depth, and the smoke in the background adds an element of mystery and intrigue.', 0, 8, 22),
	(84, 33, 2, 'The focus on the central figure in the frame is also a great touch, as it draws the viewer\'s eye to the heart of the action', 0, 3, 22),
	(85, 40, 2, 'Overall, this photo is a great representation of the surva ritual, and it really captures the energy and spirit of the tradition. ', 0, 8, 22),
	(86, 47, 2, 'The photographer has done an excellent job of creating a visually striking image that is both dynamic and compelling.', 0, 4, 22),
	(87, 25, 2, 'One thing that really stands out to me is the composition of the photo.', 0, 8, 21),
	(88, 34, 2, 'The play of light and shadow also adds depth and dimensionality to the image.', 0, 8, 21),
	(89, 41, 2, 'I also appreciate the use of color in the photo. The muted tones and earthy hues of the buildings and the surrounding foliage create a sense of warmth and nostalgia that\'s perfectly suited to the subject matter.', 0, 8, 21),
	(90, 48, 2, 'Overall, this is a beautiful photo that perfectly captures the charm and character of old houses. ', 0, 8, 21),
	(91, 27, 2, 'The way the photographer has captured the delicate beauty and intricate details of the blooms is truly impressive.', 0, 8, 20),
	(92, 35, 2, 'The soft, muted tones and gentle lighting create a sense of tranquility and serenity that\'s perfect for the subject matter.', 0, 7, 20),
	(93, 42, 2, 'One thing that really stands out to me is the composition of the photo. The way the flowers are arranged in the frame and the use of negative space create a sense of balance and harmony. ', 0, 6, 20),
	(94, 49, 2, 'The warm, earthy hues and soft pastels of the flowers and the surrounding foliage create a sense of nostalgia and romance that\'s perfectly suited to the subject matter.', 0, 3, 20),
	(95, 28, 2, 'I absolutely love this photo of the sea. The colors are so vibrant and the composition is just stunning.', 0, 8, 19),
	(96, 36, 2, 'The photographer has done a great job of capturing the motion of the waves and the way the light reflects off the water.', 0, 8, 19),
	(97, 43, 2, 'The way the rocks in the foreground are juxtaposed against the vastness of the sea in the background creates a sense of depth and perspective that draws the viewer into the image.', 0, 10, 19),
	(98, 50, 2, 'Overall, this is a beautiful photo that really captures the beauty and majesty of the sea.', 0, 3, 19),
	(99, 20, 3, 'The snowy landscape is beautifully captured and the contrast between the white snow and the blue sky is striking. ', 0, 7, 25),
	(100, 22, 3, 'The photographer has also done a great job of framing the image with the trees in the foreground, which adds depth to the picture.', 0, 4, 25),
	(101, 30, 3, 'The subtle shadows and highlights also add to the texture and create a sense of depth.', 0, 9, 25),
	(102, 38, 3, 'Overall, this is a wonderful photo that perfectly captures the beauty of winter. Well done!', 0, 10, 25),
	(103, 45, 3, 'I really enjoyed this photo of winter. The snowy landscape is beautifully captured and the contrast between the white snow and the blue sky is striking.', 0, 7, 25),
	(104, 23, 3, 'This photo of the views is absolutely breathtaking. The way the photographer has captured the sweeping landscape and the distant mountains is truly awe-inspiring.', 0, 7, 24),
	(105, 32, 3, 'One thing that really stands out to me is the use of light in the photo. The warm, golden tones of the sunlight create a sense of warmth and intimacy that draws the viewer into the image.', 0, 4, 24),
	(106, 39, 3, 'It almost feels like I\'m there, basking in the glow of the sun and taking in the stunning views.', 0, 9, 24),
	(107, 46, 3, 'The way the foreground leads the eye to the distant mountains creates a sense of depth and perspective that adds to the overall impact of the image.', 0, 8, 24),
	(108, 29, 3, ' The photographer has done a great job of capturing the beauty and tranquility of the autumn landscape.', 0, 8, 23),
	(109, 37, 3, 'The way the trees frame the image and create a sense of depth is really effective, and the use of negative space adds to the overall impact of the image.', 0, 8, 23),
	(110, 44, 3, 'I also appreciate the use of color in the photo. The warm, golden hues of the leaves and the cool blues of the sky complement each other perfectly and create a sense of balance and harmony.', 0, 3, 23),
	(111, 24, 3, 'The photographer has also done a great job of capturing the motion and energy of the performers in action.', 0, 6, 22),
	(112, 33, 3, 'The composition of the photo is also really interesting. The way the performers are arranged in the frame creates a sense of movement and depth, and the smoke in the background adds an element of mystery and intrigue. ', 0, 10, 22),
	(113, 40, 3, 'The focus on the central figure in the frame is also a great touch, as it draws the viewer\'s eye to the heart of the action.', 0, 7, 22),
	(114, 47, 3, 'Overall, this photo is a great representation of the surva ritual, and it really captures the energy and spirit of the tradition.', 0, 3, 22),
	(115, 25, 3, 'The sense of history and character is palpable.', 0, 6, 21),
	(116, 34, 3, 'The way the photographer has captured the intricate details and textures of the buildings is truly impressive. ', 0, 9, 21),
	(117, 41, 3, ' The play of light and shadow also adds depth and dimensionality to the image.', 0, 8, 21),
	(118, 48, 3, 'I really love this photo of the old houses. The way the photographer has captured the intricate details and textures of the buildings is truly impressive. The sense of history and character is palpable.', 0, 4, 21),
	(119, 27, 3, 'The soft, muted tones and gentle lighting create a sense of tranquility and serenity that\'s perfect for the subject matter.', 0, 7, 20),
	(120, 35, 3, 'The way the flowers are arranged in the frame and the use of negative space create a sense of balance and harmony.', 0, 8, 20),
	(121, 42, 3, ' The blurred background also adds depth and dimensionality to the image.', 0, 6, 20),
	(122, 49, 3, 'The photographer has done an excellent job of creating a visually stunning image that evokes a sense of peace and tranquility', 0, 10, 20),
	(123, 28, 3, 'The photographer has done a great job of capturing the motion of the waves and the way the light reflects off the water.', 0, 7, 19),
	(124, 36, 3, ' The way the rocks in the foreground are juxtaposed against the vastness of the sea in the background creates a sense of depth and perspective that draws the viewer into the image.', 0, 3, 19),
	(125, 43, 3, 'The bright blue of the sky and the deep blues and greens of the water really create a sense of tranquility and calm.', 0, 9, 19),
	(126, 50, 3, 'It almost feels like I\'m there, listening to the sound of the waves and feeling the cool ocean breeze on my face.', 0, 4, 19),
	(127, 20, 4, 'I really enjoyed this photo of winter.', 0, 8, 25),
	(128, 22, 4, 'The snowy landscape is beautifully captured and the contrast between the white snow and the blue sky is striking.', 0, 8, 25),
	(129, 30, 4, 'One thing that stands out to me is the texture of the snow.', 0, 4, 25),
	(130, 38, 4, 'The way it\'s been captured almost makes me feel like I\'m there, feeling the crisp winter air and the crunch of the snow under my feet. The subtle shadows and highlights also add to the texture and create a sense of depth.', 0, 8, 25),
	(131, 45, 4, 'Overall, this is a wonderful photo that perfectly captures the beauty of winter. Well done!', 0, 3, 25),
	(132, 23, 4, ' The sense of scale and grandeur is incredible.', 0, 8, 24),
	(133, 32, 4, 'One thing that really stands out to me is the use of light in the photo.', 0, 8, 24),
	(134, 39, 4, ' It almost feels like I\'m there, basking in the glow of the sun and taking in the stunning views.', 0, 8, 24),
	(135, 46, 4, 'The way the foreground leads the eye to the distant mountains creates a sense of depth and perspective that adds to the overall impact of the image. ', 0, 7, 24),
	(136, 29, 4, 'The warm, golden tones of the leaves and the soft, diffused light create a sense of warmth and coziness that\'s perfect for the season. The photographer has done a great job of capturing the beauty and tranquility of the autumn landscape.', 0, 10, 23),
	(137, 37, 4, 'One thing that really stands out to me is the composition of the photo. The way the trees frame the image and create a sense of depth is really effective, and the use of negative space adds to the overall impact of the image.', 0, 4, 23),
	(138, 44, 4, ' The fallen leaves on the ground also add a nice touch of texture and detail.', 0, 8, 23),
	(139, 24, 4, 'I\'m really drawn to this photo of the surva ritual. The vibrant colors of the costumes and masks are absolutely stunning and really capture the essence of the tradition.', 0, 6, 22),
	(140, 33, 4, 'The photographer has also done a great job of capturing the motion and energy of the performers in action.', 0, 7, 22),
	(141, 40, 4, ' The way the performers are arranged in the frame creates a sense of movement and depth, and the smoke in the background adds an element of mystery and intrigue. ', 0, 8, 22),
	(142, 47, 4, 'The photographer has done an excellent job of creating a visually striking image that is both dynamic and compelling.', 0, 10, 22),
	(143, 25, 4, 'The sense of history and character is palpable.', 0, 6, 21),
	(144, 34, 4, 'The way the houses are arranged in the frame and the use of negative space create a sense of balance and harmony.', 0, 10, 21),
	(145, 41, 4, 'The play of light and shadow also adds depth and dimensionality to the image.', 0, 8, 21),
	(146, 48, 4, 'The muted tones and earthy hues of the buildings and the surrounding foliage create a sense of warmth and nostalgia that\'s perfectly suited to the subject matter.', 0, 3, 21),
	(147, 27, 4, 'The soft, muted tones and gentle lighting create a sense of tranquility and serenity that\'s perfect for the subject matter', 0, 6, 20),
	(148, 35, 4, 'The way the flowers are arranged in the frame and the use of negative space create a sense of balance and harmony. ', 0, 8, 20),
	(149, 42, 4, 'The warm, earthy hues and soft pastels of the flowers and the surrounding foliage create a sense of nostalgia and romance that\'s perfectly suited to the subject matter.', 0, 8, 20),
	(150, 49, 4, 'Overall, this is a beautiful photo that perfectly captures the beauty and fragility of old flowers. ', 0, 7, 20),
	(151, 28, 4, 'I absolutely love this photo of the sea. The colors are so vibrant and the composition is just stunning.', 0, 10, 19),
	(152, 36, 4, 'The photographer has done a great job of capturing the motion of the waves and the way the light reflects off the water.', 0, 4, 19),
	(153, 43, 4, 'The way the rocks in the foreground are juxtaposed against the vastness of the sea in the background creates a sense of depth and perspective that draws the viewer into the image.', 0, 8, 19),
	(154, 50, 4, ' The colors are so vibrant and the composition is just stunning. The photographer has done a great job of capturing the motion of the waves and the way the light reflects off the water.', 0, 8, 19);
/*!40000 ALTER TABLE `photo_reviews` ENABLE KEYS */;

-- Dumping data for table photo_contest.places: ~3 rows (approximately)
/*!40000 ALTER TABLE `places` DISABLE KEYS */;
INSERT INTO `places` (`place_id`, `place_name`) VALUES
	(1, 'Golden'),
	(2, 'Silver'),
	(3, 'Bronze');
/*!40000 ALTER TABLE `places` ENABLE KEYS */;

-- Dumping data for table photo_contest.ranks: ~4 rows (approximately)
/*!40000 ALTER TABLE `ranks` DISABLE KEYS */;
INSERT INTO `ranks` (`rank_id`, `rank_name`, `min_score`, `max_score`) VALUES
	(1, 'Junkie', 0, 50),
	(2, 'Enthusiast', 51, 150),
	(3, 'Master', 151, 1000),
	(4, 'Photo Dictator', 1001, 50000);
/*!40000 ALTER TABLE `ranks` ENABLE KEYS */;

-- Dumping data for table photo_contest.roles: ~2 rows (approximately)
/*!40000 ALTER TABLE `roles` DISABLE KEYS */;
INSERT INTO `roles` (`role_id`, `role_name`) VALUES
	(1, 'Organizer'),
	(2, 'Photo Junkie');
/*!40000 ALTER TABLE `roles` ENABLE KEYS */;

-- Dumping data for table photo_contest.users: ~16 rows (approximately)
/*!40000 ALTER TABLE `users` DISABLE KEYS */;
INSERT INTO `users` (`user_id`, `first_name`, `last_name`, `email`, `username`, `password`, `role_id`, `created_at`, `rank_id`, `total_score`, `reset_password_token`, `isInactive`) VALUES
	(1, 'Deleted user', 'Deleted user', 'DeletedUser@abv.bg', 'Deleted user', 'Deleted user', 2, '2023-04-07 12:10:26', 1, 0, NULL, 0),
	(2, 'Chavdar', 'Georgiev', 'chavo@mail.com', 'chavo155', '$2a$12$V.XsD7CbCMCC/RDSzx0nMOWBbf4wSF4iP9eHBDHOlE96GgcwzD83y', 1, '2023-04-07 12:16:56', 1, 0, NULL, 0),
	(3, 'Georgi', 'Simeonov', 'gogo256@mail.bg', 'gogo_pic', '$2a$12$V.XsD7CbCMCC/RDSzx0nMOWBbf4wSF4iP9eHBDHOlE96GgcwzD83y', 1, '2023-04-07 12:16:56', 1, 0, NULL, 0),
	(4, 'Ivan', 'Todorov', 'vankata56@gmail.com', 'ivan_59', '$2a$12$V.XsD7CbCMCC/RDSzx0nMOWBbf4wSF4iP9eHBDHOlE96GgcwzD83y', 1, '2023-04-07 12:16:56', 1, 0, NULL, 0),
	(5, 'Drago', 'Nikolov', 'dragomir@abv.bg', 'drago_photo', '$2a$12$V.XsD7CbCMCC/RDSzx0nMOWBbf4wSF4iP9eHBDHOlE96GgcwzD83y', 2, '2023-04-07 12:16:56', 3, 256, NULL, 0),
	(6, 'Ivo', 'Asenov', 'ivaylo_98@mail.com', 'basic_user', '$2a$12$V.XsD7CbCMCC/RDSzx0nMOWBbf4wSF4iP9eHBDHOlE96GgcwzD83y', 2, '2023-04-07 12:16:56', 3, 356, NULL, 0),
	(7, 'Gergana', 'Traikov', 'geri.t@mail.com', 'geri_88', '$2a$12$V.XsD7CbCMCC/RDSzx0nMOWBbf4wSF4iP9eHBDHOlE96GgcwzD83y', 2, '2023-04-07 12:16:56', 3, 186, NULL, 0),
	(8, 'Petar', 'Vasilev', 'pesho_v@abv.bg', 'pesho.vas', '$2a$12$V.XsD7CbCMCC/RDSzx0nMOWBbf4wSF4iP9eHBDHOlE96GgcwzD83y', 2, '2023-04-07 12:16:56', 1, 1, NULL, 0),
	(9, 'Elitsa', 'Petrov', 'eli_tsa@mail.com', 'elitsa896', '$2a$12$V.XsD7CbCMCC/RDSzx0nMOWBbf4wSF4iP9eHBDHOlE96GgcwzD83y', 2, '2023-04-07 12:16:56', 1, 1, NULL, 0),
	(10, 'Samuil', 'Valkov', 'sami_m@gmail.com', 'sami_m', '$2a$12$V.XsD7CbCMCC/RDSzx0nMOWBbf4wSF4iP9eHBDHOlE96GgcwzD83y', 2, '2023-04-07 12:16:56', 1, 11, NULL, 0),
	(11, 'Kaloyan', 'Dimitrov', 'koko_21@abv.bg', 'kaloyan_d', '$2a$12$V.XsD7CbCMCC/RDSzx0nMOWBbf4wSF4iP9eHBDHOlE96GgcwzD83y', 2, '2023-04-07 12:16:56', 1, 26, NULL, 0),
	(15, 'Georgi', 'Ivanov', 'kirilova.gergana55@gmail.com', 'new_user', '$2a$12$V.XsD7CbCMCC/RDSzx0nMOWBbf4wSF4iP9eHBDHOlE96GgcwzD83y', 2, '2023-04-27 13:27:45', 2, 101, '639b6dc9-7f8f-407c-b357-8c66a06508a4', 0),
	(16, 'Vladimir', 'Todorov', 'vlado32@abv.bg', 'organizer_user', '$2a$12$V.XsD7CbCMCC/RDSzx0nMOWBbf4wSF4iP9eHBDHOlE96GgcwzD83y', 1, '2023-04-27 13:34:20', 1, 0, NULL, 0),
	(17, 'Teodor', 'Georgiev', 'tosho@abv.bg', 'tosho_new', '$2a$12$V.XsD7CbCMCC/RDSzx0nMOWBbf4wSF4iP9eHBDHOlE96GgcwzD83y', 2, '2023-04-27 22:32:51', 1, 1, NULL, 0),
	(18, 'Kostadin', 'Kirilov', 'koko@av.bg', 'master_user', '$2a$12$V.XsD7CbCMCC/RDSzx0nMOWBbf4wSF4iP9eHBDHOlE96GgcwzD83y', 2, '2023-04-27 22:33:24', 3, 160, NULL, 0),
	(19, 'Trifon', 'Vandalov', 'tri_fon@abv.bg', 'master_user1', '$2a$12$V.XsD7CbCMCC/RDSzx0nMOWBbf4wSF4iP9eHBDHOlE96GgcwzD83y', 2, '2023-04-27 22:34:09', 3, 160, NULL, 0);
/*!40000 ALTER TABLE `users` ENABLE KEYS */;

-- Dumping data for table photo_contest.winner_photos: ~30 rows (approximately)
/*!40000 ALTER TABLE `winner_photos` DISABLE KEYS */;
INSERT INTO `winner_photos` (`winner_photo_id`, `contest_photo_id`, `place_id`, `points_winners`) VALUES
	(19, 12, 1, 50),
	(20, 14, 2, 25),
	(21, 13, 2, 25),
	(22, 15, 3, 10),
	(23, 19, 3, 10),
	(24, 43, 1, 50),
	(25, 28, 2, 35),
	(26, 50, 3, 10),
	(27, 36, 3, 10),
	(28, 35, 1, 50),
	(29, 42, 2, 25),
	(30, 49, 2, 25),
	(31, 27, 2, 25),
	(32, 34, 1, 50),
	(33, 41, 2, 35),
	(34, 25, 3, 20),
	(35, 33, 1, 40),
	(36, 24, 1, 40),
	(37, 40, 2, 25),
	(38, 47, 2, 25),
	(39, 29, 1, 50),
	(40, 44, 2, 35),
	(41, 37, 3, 20),
	(42, 39, 1, 50),
	(43, 46, 2, 35),
	(44, 23, 3, 20),
	(45, 22, 1, 50),
	(46, 38, 2, 25),
	(47, 20, 2, 25),
	(48, 30, 3, 20);
/*!40000 ALTER TABLE `winner_photos` ENABLE KEYS */;

/*!40101 SET SQL_MODE=IFNULL(@OLD_SQL_MODE, '') */;
/*!40014 SET FOREIGN_KEY_CHECKS=IFNULL(@OLD_FOREIGN_KEY_CHECKS, 1) */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40111 SET SQL_NOTES=IFNULL(@OLD_SQL_NOTES, 1) */;
