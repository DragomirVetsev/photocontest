package com.example.photocontest.helpers;

import org.springframework.security.crypto.bcrypt.BCrypt;

public class PasswordEncoder {
    private static final int SALT_ROUNDS = 12;

    public static String encode(String password) {
        String salt = BCrypt.gensalt(SALT_ROUNDS);
        return BCrypt.hashpw(password, salt);
    }

    public static boolean matches(String password, String hashedPassword) {
        return BCrypt.checkpw(password, hashedPassword);
    }
}
