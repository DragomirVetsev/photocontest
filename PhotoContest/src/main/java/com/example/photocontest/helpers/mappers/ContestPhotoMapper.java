package com.example.photocontest.helpers.mappers;

import com.example.photocontest.models.ContestPhoto;
import com.example.photocontest.models.User;
import com.example.photocontest.models.dto.ContestPhotoOutputDto;
import com.example.photocontest.services.contracts.ContestService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.List;
import java.util.stream.Collectors;

@Component
public class ContestPhotoMapper {
    ContestService contestService;

    @Autowired
    public ContestPhotoMapper(ContestService contestService) {
        this.contestService = contestService;
    }

    public ContestPhotoOutputDto toOutputDto(ContestPhoto contestPhoto) {

        ContestPhotoOutputDto contestPhotoOutputDto = new ContestPhotoOutputDto();

        contestPhotoOutputDto.setContestName(contestPhoto.getContest().getTitle());
        contestPhotoOutputDto.setTitle(contestPhoto.getTitle());
        contestPhotoOutputDto.setStory(contestPhoto.getStory());
        contestPhotoOutputDto.setPhotoUrl(contestPhoto.getPhotoUrl());
        contestPhotoOutputDto.setUsername(contestPhoto.getUser().getUsername());
        contestPhotoOutputDto.setCreatedAt(contestPhoto.getCreatedAt());

        return contestPhotoOutputDto;
    }

    public List<ContestPhotoOutputDto> toOutputListDto(List<ContestPhoto> contestPhotos) {
        return contestPhotos.stream()
                .map(this::toOutputDto)
                .collect(Collectors.toList());
    }

    public ContestPhoto contestPhotoToUpload(User user, int contestId, String photoTitle, String story, String fileName) {
        ContestPhoto contestPhoto = new ContestPhoto();

        contestPhoto.setUser(user);
        contestPhoto.setContest(contestService.getById(contestId));
        contestPhoto.setTitle(photoTitle);
        contestPhoto.setStory(story);
        contestPhoto.setPhotoUrl(fileName);

        return contestPhoto;
    }
}
