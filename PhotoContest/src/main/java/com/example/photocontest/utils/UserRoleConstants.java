package com.example.photocontest.utils;

public final class UserRoleConstants {

    public static final String ORGANIZER = "Organizer";
    public static final String PHOTO_JUNKIE = "Photo Junkie";
    public static final String JUROR = "Juror";
    public static final String CAN_FOUND_ROLE_ORGANIZER = "Can found role Organizer";

    private UserRoleConstants() {
    }

}
