package com.example.photocontest.models.dto;

public class ForgotPasswordDto {

    private String email;

    public ForgotPasswordDto() {
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }
}
