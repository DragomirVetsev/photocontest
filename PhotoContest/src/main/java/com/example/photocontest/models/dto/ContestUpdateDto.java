package com.example.photocontest.models.dto;

import jakarta.persistence.Column;
import jakarta.validation.constraints.NotEmpty;
import jakarta.validation.constraints.Size;

public class ContestUpdateDto {

    @NotEmpty(message = "Title can't be empty")
    @Size(min = 2, max = 50, message = "Name should be between 2 and 20 symbols")
    private String title;

    @Column(name = "cover_photo_name")
    @NotEmpty(message = "Cover photo name can't be empty")
    private String coverPhotoName;


    public ContestUpdateDto() {
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getCoverPhotoName() {
        return coverPhotoName;
    }

    public void setCoverPhotoName(String coverPhotoName) {
        this.coverPhotoName = coverPhotoName;
    }

}
