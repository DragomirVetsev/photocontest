package com.example.photocontest.models.dto;

import com.example.photocontest.models.ContestPhoto;

public class ContestPhotoAndPointsDto {
    ContestPhoto contestPhoto;
    double score;

    public ContestPhotoAndPointsDto(ContestPhoto contestPhoto, Double score) {
        this.contestPhoto = contestPhoto;
        this.score = score;
    }

    public ContestPhoto getContestPhoto() {
        return contestPhoto;
    }

    public void setContestPhoto(ContestPhoto contestPhoto) {
        this.contestPhoto = contestPhoto;
    }

    public double getScore() {
        return score;
    }

    public void setScore(double score) {
        this.score = score;
    }
}
