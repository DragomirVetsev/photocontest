package com.example.photocontest.models;

import java.util.Optional;

public class ContestPhotoFilterOptions {
    private final Optional<String> title;
    private final Optional<String> sortBy;
    private final Optional<String> sortOrder;

    public ContestPhotoFilterOptions() {
        this(null, null, null);
    }

    public ContestPhotoFilterOptions(String title, String sortBy, String sortOrder) {
        this.title = Optional.ofNullable(title);
        this.sortBy = Optional.ofNullable(sortBy);
        this.sortOrder = Optional.ofNullable(sortOrder);
    }

    public Optional<String> getSortBy() {
        return sortBy;
    }

    public Optional<String> getSortOrder() {
        return sortOrder;
    }

    public Optional<String> getTitle() {
        return title;
    }
}
