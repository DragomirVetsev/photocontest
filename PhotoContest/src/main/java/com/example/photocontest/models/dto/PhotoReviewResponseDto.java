package com.example.photocontest.models.dto;

public class PhotoReviewResponseDto {
    private int contestId;

    private int contestPhotoId;

    private String jurorUsername;

    private String comment;

    private int score;

    public PhotoReviewResponseDto() {
    }

    public int getContestId() {
        return contestId;
    }

    public void setContestId(int contestId) {
        this.contestId = contestId;
    }

    public int getContestPhotoId() {
        return contestPhotoId;
    }

    public void setContestPhotoId(int contestPhotoId) {
        this.contestPhotoId = contestPhotoId;
    }

    public String getJurorUsername() {
        return jurorUsername;
    }

    public void setJurorUsername(String jurorUsername) {
        this.jurorUsername = jurorUsername;
    }

    public String getComment() {
        return comment;
    }

    public void setComment(String comment) {
        this.comment = comment;
    }

    public int getScore() {
        return score;
    }

    public void setScore(int score) {
        this.score = score;
    }
}
