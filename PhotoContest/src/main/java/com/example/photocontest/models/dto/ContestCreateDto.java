package com.example.photocontest.models.dto;

import jakarta.validation.constraints.NotEmpty;
import jakarta.validation.constraints.NotNull;
import jakarta.validation.constraints.Size;

import java.time.LocalDateTime;
import java.util.Set;

public class ContestCreateDto {

    @NotEmpty(message = "Title can't be empty")
    @Size(min = 2, max = 50, message = "Name should be between 2 and 20 symbols")
    private String title;

    @NotEmpty(message = "Category can't be empty")
    private String categoryName;

    @NotNull(message = "Cover photo can`t be empty")
    private String coverPhotoName;

    @NotNull(message = "Phase one end can't be empty")
    private LocalDateTime phaseOneEnd;

    @NotNull(message = "Phase two end can`t be empty")
    private LocalDateTime phaseTwoEnd;

    private Set<String> juryMembers;

    private boolean isInvitational;
    private Set<String> invitedUsers;

    public ContestCreateDto() {
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getCategoryName() {
        return categoryName;
    }

    public void setCategoryName(String categoryName) {
        this.categoryName = categoryName;
    }

    public LocalDateTime getPhaseOneEnd() {
        return phaseOneEnd;
    }

    public void setPhaseOneEnd(LocalDateTime phaseOneEnd) {
        this.phaseOneEnd = phaseOneEnd;
    }

    public boolean isInvitational() {
        return isInvitational;
    }

    public void setInvitational(boolean invitational) {
        isInvitational = invitational;
    }

    public Set<String> getInvitedUsers() {
        return invitedUsers;
    }

    public void setInvitedUsers(Set<String> invitedUsers) {
        this.invitedUsers = invitedUsers;
    }

    public LocalDateTime getPhaseTwoEnd() {
        return phaseTwoEnd;
    }

    public void setPhaseTwoEnd(LocalDateTime phaseTwoEnd) {
        this.phaseTwoEnd = phaseTwoEnd;
    }

    public String getCoverPhotoName() {
        return coverPhotoName;
    }

    public void setCoverPhotoName(String coverPhotoName) {
        this.coverPhotoName = coverPhotoName;
    }

    public Set<String> getJuryMembers() {
        return juryMembers;
    }

    public void setJuryMembers(Set<String> juryMembers) {
        this.juryMembers = juryMembers;
    }
}
