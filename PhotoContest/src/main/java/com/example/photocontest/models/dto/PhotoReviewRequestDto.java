package com.example.photocontest.models.dto;

import jakarta.validation.constraints.Max;
import jakarta.validation.constraints.Min;
import jakarta.validation.constraints.NotNull;

public class PhotoReviewRequestDto {

    @NotNull(message = "Contest id can't be empty")
    private int contestId;

    @NotNull(message = "Photo id can't be empty")
    private int contestPhotoId;

    @NotNull(message = "Is wrong category can't be empty")
    private boolean isWrongCategory;

    private String comment;

    @NotNull(message = "Score can't be empty")
    @Min(value = 0, message = "Score must be between 1 and 10")
    @Max(value = 10, message = "Score must be between 1 and 10")
    private int score;

    public PhotoReviewRequestDto() {
    }

    public int getContestId() {
        return contestId;
    }

    public void setContestId(int contestId) {
        this.contestId = contestId;
    }

    public int getContestPhotoId() {
        return contestPhotoId;
    }

    public void setContestPhotoId(int contestPhotoId) {
        this.contestPhotoId = contestPhotoId;
    }

    public boolean isWrongCategory() {
        return isWrongCategory;
    }

    public void setWrongCategory(boolean wrongCategory) {
        isWrongCategory = wrongCategory;
    }

    public String getComment() {
        return comment;
    }

    public void setComment(String comment) {
        this.comment = comment;
    }

    public int getScore() {
        return score;
    }

    public void setScore(int score) {
        this.score = score;
    }
}
