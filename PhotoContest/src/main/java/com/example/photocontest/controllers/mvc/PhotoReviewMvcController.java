package com.example.photocontest.controllers.mvc;

import com.example.photocontest.exceptions.InvalidInputOperation;
import com.example.photocontest.exceptions.UnauthorizedOperationException;
import com.example.photocontest.helpers.AuthenticationHelper;
import com.example.photocontest.helpers.mappers.PhotoReviewMapper;
import com.example.photocontest.models.Contest;
import com.example.photocontest.models.ContestPhoto;
import com.example.photocontest.models.PhotoReview;
import com.example.photocontest.models.User;
import com.example.photocontest.models.dto.PhotoReviewRequestDto;
import com.example.photocontest.services.contracts.ContestPhotoService;
import com.example.photocontest.services.contracts.PhotoReviewService;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpSession;
import jakarta.validation.Valid;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;

@Controller
@RequestMapping("/contests/{contestId}/contest-photos/{contestPhotoId}")
public class PhotoReviewMvcController {

    private final AuthenticationHelper authenticationHelper;

    private final PhotoReviewService photoReviewService;
    private final PhotoReviewMapper photoReviewMapper;

    private final ContestPhotoService contestPhotoService;


    @Autowired
    public PhotoReviewMvcController(AuthenticationHelper authenticationHelper,
                                    PhotoReviewService photoReviewService,
                                    PhotoReviewMapper photoReviewMapper, ContestPhotoService contestPhotoService) {
        this.authenticationHelper = authenticationHelper;
        this.photoReviewService = photoReviewService;
        this.photoReviewMapper = photoReviewMapper;
        this.contestPhotoService = contestPhotoService;
    }

    @GetMapping("/review")
    public String showNewReviewPage(@Valid @ModelAttribute("photoReview") PhotoReviewRequestDto photoReviewRequestDto,
                                    BindingResult bindingResult,
                                    Model model, HttpSession session) {
        try {
            authenticationHelper.tryGetCurrentUser(session);
        } catch (UnauthorizedOperationException e) {
            return "redirect:/auth/login";
        }


        if (bindingResult.hasErrors()) {
            return "ContestPhotoReviewCreateView";
        }

        model.addAttribute("photoReview", new PhotoReviewRequestDto());
        return "ContestPhotoReviewCreateView";
    }

    @PostMapping("/review")
    public String createPhotoReview(@Valid @ModelAttribute("photoReview") PhotoReviewRequestDto photoReviewRequestDto,
                                    BindingResult bindingResult,
                                    HttpServletRequest request,
                                    @PathVariable int contestId,
                                    @PathVariable int contestPhotoId,
                                    Model model,
                                    HttpSession session) {
        User user;
        try {
            user = authenticationHelper.tryGetCurrentUser(session);
        } catch (UnauthorizedOperationException e) {
            return "redirect:/auth/login";
        }

        model.addAttribute("requestURI", request.getRequestURI());

        if (bindingResult.hasErrors()) {
            return "ContestPhotoReviewCreateView";
        }

        try {
            photoReviewRequestDto.setContestId(contestId);
            photoReviewRequestDto.setContestPhotoId(contestPhotoId);
            ContestPhoto contestPhoto = contestPhotoService.get(contestPhotoId);
            PhotoReview photoReview = photoReviewMapper.fromCreateDto(photoReviewRequestDto, user);
            photoReviewService.create(photoReview, contestPhoto, user);
            Contest contest = contestPhoto.getContest();
            model.addAttribute("contestPhoto", contestPhoto);
            model.addAttribute("contest", contest);


            return "redirect:/contests/{contestId}/contest-photos/{contestPhotoId}";
        } catch (UnauthorizedOperationException e) {
            model.addAttribute("error", e.getMessage());
            return "UnauthorizedView";
        } catch (InvalidInputOperation e) {
            model.addAttribute("error", e.getMessage());
            return "ContestPhotoReviewCreateView";
        }
    }
}


