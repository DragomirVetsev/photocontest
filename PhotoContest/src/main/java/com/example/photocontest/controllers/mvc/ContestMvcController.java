package com.example.photocontest.controllers.mvc;

import com.example.photocontest.exceptions.*;
import com.example.photocontest.helpers.AuthenticationHelper;
import com.example.photocontest.helpers.mappers.ContestMapper;
import com.example.photocontest.helpers.mappers.ContestPhotoMapper;
import com.example.photocontest.helpers.mappers.PhotoReviewMapper;
import com.example.photocontest.models.*;
import com.example.photocontest.models.dto.*;
import com.example.photocontest.services.contracts.*;
import jakarta.persistence.EntityExistsException;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpSession;
import jakarta.validation.Valid;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import java.time.LocalDateTime;
import java.util.List;
import java.util.stream.Collectors;

@Controller
@RequestMapping("/contests")
public class ContestMvcController {

    private final ContestService contestService;
    private final ContestMapper contestMapper;
    private final WinnerPhotosService winnerPhotosService;
    private final ContestPhotoService contestPhotoService;
    private final AuthenticationHelper authenticationHelper;
    private final UserService userService;
    private final ContestPhotoMapper contestPhotoMapper;
    private final CategoryService categoryService;
    private final ContestPhotoService contestPhotosService;
    private final PhotoReviewService photoReviewService;
    private final PhotoReviewMapper photoReviewMapper;

    @Autowired
    public ContestMvcController(ContestService contestService, WinnerPhotosService winnerPhotosService,
                                ContestMapper contestMapper, ContestPhotoService contestPhotoService,
                                AuthenticationHelper authenticationHelper, UserService userService,
                                CategoryService categoryService, ContestPhotoMapper contestPhotoMapper,
                                ContestPhotoService contestPhotosService, PhotoReviewService photoReviewService,
                                PhotoReviewMapper photoReviewMapper) {
        this.contestService = contestService;
        this.contestMapper = contestMapper;
        this.contestPhotoService = contestPhotoService;
        this.winnerPhotosService = winnerPhotosService;
        this.authenticationHelper = authenticationHelper;
        this.userService = userService;
        this.categoryService = categoryService;
        this.contestPhotoMapper = contestPhotoMapper;
        this.contestPhotosService = contestPhotosService;
        this.photoReviewService = photoReviewService;
        this.photoReviewMapper = photoReviewMapper;
    }

    @ModelAttribute("isAuthenticated")
    public boolean populateIsAuthenticated(HttpSession session) {
        return session.getAttribute("currentUser") != null;
    }

    @ModelAttribute("currentUserName")
    public String populateCurrentUserName(HttpSession session) {
        return (String) session.getAttribute("currentUser");
    }

    @ModelAttribute("isOrganizer")
    public boolean isOrganizer(HttpSession session) {
        User user;
        try {
            user = userService.getUserByUsername(populateCurrentUserName(session));
            return userService.isOrganizer(user);
        } catch (EntityNotFoundException e) {
            return false;
        }
    }

    @ModelAttribute("currentUser")
    public User getCurrentUser(HttpSession session) {
        User user;
        try {
            user = userService.getUserByUsername(populateCurrentUserName(session));
            return user;
        } catch (EntityNotFoundException e) {
            return null;
        }
    }

    @GetMapping("/{id}")
    public String showSingleContest(@Valid @PathVariable int id, Model model, HttpSession session) {

        try {
            authenticationHelper.tryGetCurrentUser(session);
            Contest contest = contestService.getById(id);
            ContestOutDto contestOutDto = contestMapper.contestToOutDto(contest);
            List<ContestPhoto> contestPhotos = contestPhotoService.getContestPhotosByContestId(id);

            model.addAttribute("contest", contest);
            model.addAttribute("contestDto", contestOutDto);
            model.addAttribute("contestPhotos", contestPhotos);
            model.addAttribute("jury", contestService.getContestJuryByContestId(id));
            model.addAttribute("winnerPhotos", winnerPhotosService.getWinnerPhotosByContest(contest));

            List<PhotoReview> photoReviews = photoReviewService.getAllByContest(contest);

            List<ContestPhotoAndPointsDto> contestPhotoAndPointsDtos =
                    photoReviewMapper.toContestPhotoAndPointsDto(photoReviews);

            model.addAttribute("contestPhotosWithPoints", contestPhotoAndPointsDtos);

            if (contestOutDto.getCurrentPhase().equals("First phase")) {
                model.addAttribute("dueDate", contestMapper.contestToOutDto(contest).getPhaseTwoStart());
            } else if (contestOutDto.getCurrentPhase().equals("Second phase")) {
                model.addAttribute("dueDate", contestMapper.contestToOutDto(contest).getFinishedDate());
            }

            return "ContestView";
        } catch (EntityNotFoundException e) {
            model.addAttribute("error", e.getMessage());
            return "NotFoundView";
        } catch (UnauthorizedOperationException e) {
            return "redirect:/auth/login";
        }
    }

    @GetMapping("/upload-photo")
    public String uploadPhotoTest() {
        return "UploadPhotoView";
    }

    @GetMapping("/phase-one")
    public String contestsInPhaseOne(Model model, HttpSession session) {
        User user;
        try {
            user = authenticationHelper.tryGetCurrentUser(session);
        } catch (UnauthorizedOperationException e) {
            return "redirect:/auth/login";
        }
        model.addAttribute("contests", contestMapper.contestsToOutDto(contestService.getContestPhaseOne(LocalDateTime.now(), user)));
        return "ContestPhaseOneView";
    }

    @GetMapping("/phase-two")
    public String contestsInPhaseTwo(Model model, HttpSession session) {
        User user;
        try {
            user = authenticationHelper.tryGetCurrentUser(session);
        } catch (UnauthorizedOperationException e) {
            return "redirect:/auth/login";
        }
        model.addAttribute("contests", contestMapper.contestsToOutDto(contestService.getContestPhaseTwo(LocalDateTime.now(), user)));
        return "ContestPhaseTwoView";
    }

    @GetMapping("/finished")
    public String contestsFinished(Model model, HttpSession session) {
        User user;
        try {
            user = authenticationHelper.tryGetCurrentUser(session);
        } catch (UnauthorizedOperationException e) {
            return "redirect:/auth/login";
        }
        model.addAttribute("contests", contestMapper.contestsToOutDto(contestService.getContestPhaseFinished(LocalDateTime.now(), user)));
        return "ContestFinishedView";
    }

    @GetMapping("/{id}/calculate-winners")
    public String makeContestWinners(@Valid @PathVariable int id) {
        try {
            winnerPhotosService.createWinners(id);
        } catch (EntityExistsException e) {
            throw new EntityNotFoundException(e.getMessage());
        }
        return "redirect:/contests/{id}";
    }


    @GetMapping
    public String showAllContests(@ModelAttribute("filterOptions") ContestFilterDto contestFilterDto, Model model, HttpSession session) {
        try {
            User user = authenticationHelper.tryGetCurrentUser(session);
            ContestFilterOptions contestFilterOptions = new ContestFilterOptions(
                    contestFilterDto.getTitle(),
                    contestFilterDto.getCategoryId(),
                    contestFilterDto.getPhase(),
                    contestFilterDto.getSortBy(),
                    contestFilterDto.getSortOrder());
            List<Contest> contests = contestService.get(contestFilterOptions);
            model.addAttribute("filterOptions", contestFilterDto);
            model.addAttribute("contests", contestMapper.contestsToOutDto(contests));
            model.addAttribute("categories", categoryService.getAll());
            model.addAttribute("currentUser", user);
            return "ContestsView";
        } catch (UnauthorizedOperationException e) {
            return "UnauthorizedView";
        }
    }

    @GetMapping("/new")
    public String showNewContestPage(@Valid @ModelAttribute("category") Category categoryModel, BindingResult bindingResult, Model model, HttpSession session) {
        User currentUser;
        currentUser = authenticationHelper.tryGetCurrentUser(session);
        contestService.checkUserPermission(currentUser);

        if (bindingResult.hasErrors()) {
            return "CreateContestView";
        }
        model.addAttribute("categories", categoryService.getAll());
        model.addAttribute("users", userService.getAll().stream()
                .filter(user -> user.getRole().getName().equals("Photo Junkie") && !user.getUsername().equals("Deleted user"))
                .collect(Collectors.toList()));
        model.addAttribute("juryForSelect", userService.getUsersByRank(currentUser, "Master"));
        model.addAttribute("contest", new ContestCreateDto());
        return "CreateContestView";
    }

    @PostMapping("/new")
    public String createContest(@Valid @ModelAttribute("contest") ContestCreateDto contestCreateDto,
                                BindingResult bindingResult,
                                Category category,
                                HttpServletRequest request,
                                Model model,
                                HttpSession session) {
        User user;
        try {
            user = authenticationHelper.tryGetCurrentUser(session);
        } catch (UnauthorizedOperationException e) {
            return "redirect:/auth/login";
        }

        model.addAttribute("requestURI", request.getRequestURI());

        if (bindingResult.hasErrors()) {
            return "CreateContestView";
        }

        try {
            Contest contest = contestMapper.fromDtoCreate(contestCreateDto, user);
            model.addAttribute("category", category);
            model.addAttribute("categories", categoryService.getAll());
            contestService.create(contest, user);
            return "redirect:/contests";
        } catch (EntityNotFoundException e) {
            model.addAttribute("error", e.getMessage());
            return "NotFoundView";
        } catch (DuplicateEntityException e) {
            bindingResult.rejectValue("title", "duplicate_contest", e.getMessage());
            return "CreateContestView";
        } catch (UnauthorizedOperationException | InvalidInputOperation | InvalidUploadException e) {
            model.addAttribute("error", e.getMessage());
            return "UnauthorizedView";
        } catch (UnsupportedOperationException e) {
            return "CreateContestInvitedErrorView";
        }
    }


    @GetMapping("/category/new")
    public String showNewCategoryPage(Model model, HttpServletRequest request, HttpSession session) {
        try {
            authenticationHelper.tryGetCurrentUser(session);
        } catch (UnauthorizedOperationException e) {
            return "UnauthorizedView";
        }
        model.addAttribute("requestURI", request.getRequestURI());
        model.addAttribute("category", new Category());
        return "CreateCategoryView";
    }

    @PostMapping("/category/new")
    public String createCategory(@Valid @ModelAttribute("category") Category category,
                                 BindingResult bindingResult,
                                 HttpServletRequest request,
                                 Model model,
                                 HttpSession session) {
        User user;
        try {
            user = authenticationHelper.tryGetCurrentUser(session);
        } catch (UnauthorizedOperationException e) {
            return "redirect:/auth/login";
        }

        model.addAttribute("requestURI", request.getRequestURI());

        if (bindingResult.hasErrors()) {
            return "CreateContestView";
        }
        try {
            categoryService.create(category, user);
            return "redirect:/contests/new";
        } catch (EntityNotFoundException e) {
            model.addAttribute("error", e.getMessage());
            return "NotFoundView";
        } catch (DuplicateEntityException e) {
            bindingResult.rejectValue("name", "duplicate_category", e.getMessage());
            return "redirect:/contests/new";
        } catch (UnauthorizedOperationException e) {
            model.addAttribute("error", e.getMessage());
            return "UnauthorizedView";
        }
    }


    @GetMapping("/{contestId}/update")
    public String showEditContestPage(HttpServletRequest request, @PathVariable int contestId, Model model, HttpSession session) {
        User currentUser;
        try {
            currentUser = authenticationHelper.tryGetCurrentUser(session);
        } catch (UnauthorizedOperationException e) {
            return "redirect:/auth/login";
        }
        try {
            Contest contest = contestService.getById(contestId);
            contestService.checkUserPermission(currentUser);
            ContestUpdateDto contestUpdateDto = contestMapper.toDtoUpdate(contest);
            model.addAttribute("requestURI", request.getRequestURI());
            model.addAttribute("contestId", contest.getId());
            model.addAttribute("contest", contestUpdateDto);
            return "UpdateContestView";
        } catch (EntityNotFoundException e) {
            model.addAttribute("error", e.getMessage());
            return "NotFoundView";
        }
    }

    @PostMapping("/{contestId}/update")
    public String contestUpdate(@Valid @PathVariable int contestId,
                                @ModelAttribute("contest") ContestUpdateDto contestUpdateDto,
                                BindingResult bindingResult,
                                Model model,
                                HttpSession session) {
        User user;
        try {
            user = authenticationHelper.tryGetCurrentUser(session);
        } catch (UnauthorizedOperationException e) {
            return "redirect:/auth/login";
        }
        if (bindingResult.hasErrors()) {
            return "UpdateContestView";
        }
        try {
            Contest contest = contestMapper.fromDtoUpdate(contestId, contestUpdateDto);
            contestService.update(contest, user);
            return "redirect:/contests";
        } catch (EntityNotFoundException e) {
            model.addAttribute("error", e.getMessage());
            return "NotFoundView";
        } catch (UnauthorizedOperationException e) {
            model.addAttribute("error", e.getMessage());
            return "UnauthorizedView";
        } catch (InvalidInputOperation e) {
            model.addAttribute("error", e.getMessage());
            return "redirect:/contests/{id}/update";
        } catch (DuplicateEntityException e) {
            bindingResult.rejectValue("title", "duplicate_contest", e.getMessage());
            return "UpdateContestView";
        }
    }

    @GetMapping("/{id}/delete")
    public String deleteContest(@PathVariable int id, Model model, HttpSession session) {
        User user;
        try {
            user = authenticationHelper.tryGetCurrentUser(session);
        } catch (UnauthorizedOperationException e) {
            return "redirect:/auth/login";
        }

        try {
            contestService.delete(id, user);

            return "redirect:/contests";
        } catch (EntityNotFoundException e) {
            model.addAttribute("error", e.getMessage());
            return "NotFoundView";
        } catch (UnauthorizedOperationException e) {
            model.addAttribute("error", e.getMessage());
            return "UnauthorizedView";
        } catch (UnsupportedOperationException e) {
            model.addAttribute("error", e.getMessage());
            return "DeleteContestView";
        }
    }

    @GetMapping("/{contestId}/upload-photo")
    public String showNewContestPhotoPage(@Valid @PathVariable int contestId, HttpServletRequest request,
                                          Model model, HttpSession session) {
        try {
            authenticationHelper.tryGetCurrentUser(session);
        } catch (UnauthorizedOperationException e) {
            model.addAttribute("error", e.getMessage());
            return "UnauthorizedView";
        }

        model.addAttribute("requestURI", request.getRequestURI());

        Contest contest = contestService.getById(contestId);
        model.addAttribute("contest", contest);

        ContestOutDto contestOutDto = contestMapper.contestToOutDto(contest);

        if (contestOutDto.getCurrentPhase().equals("First phase")) {
            model.addAttribute("dueDate", contestMapper.contestToOutDto(contest).getPhaseTwoStart());
        } else if (contestOutDto.getCurrentPhase().equals("Second phase")) {
            model.addAttribute("dueDate", contestMapper.contestToOutDto(contest).getFinishedDate());
        }
        model.addAttribute("httpServletRequest", request);
        model.addAttribute("contestPhotoInputDto", new ContestPhotoInputDto());
        return "UploadPhotoView";
    }

    @PostMapping(value = "/{contestId}/upload-photo", consumes = {"multipart/form-data"})
    public String createContestPhoto(@Valid @ModelAttribute("contestPhotoInputDto") ContestPhotoInputDto contestPhotoInputDto,
                                     BindingResult bindingResult, @PathVariable int contestId,
                                     Model model, HttpServletRequest request, HttpSession session) {

        model.addAttribute("requestURI", request.getRequestURI());
        Contest contest = contestService.getById(contestId);
        model.addAttribute("contest", contest);

        if (bindingResult.hasErrors()) {
            return "UploadPhotoView";
        }

        ContestPhoto contestPhoto;
        User user;
        try {
            user = authenticationHelper.tryGetCurrentUser(session);

            String fileName = contestPhotoInputDto.getFile().getOriginalFilename();
            contestPhoto = contestPhotoMapper.contestPhotoToUpload(user, contestId, contestPhotoInputDto.getPhotoTitle(),
                    contestPhotoInputDto.getStory(), fileName);

            contestPhotosService.createContestPhoto(contestPhoto, contestPhotoInputDto.getFile());
            contestPhotosService.setUserPointsForParticipation(user, contestId);


        } catch (EntityNotFoundException e) {
            model.addAttribute("error", e.getMessage());
            return "NotFoundView";
        } catch (AuthenticationFailureException e) {
            return "redirect:/auth/login";
        } catch (IllegalArgumentException | DuplicateEntityException | FileUploadException e) {
            bindingResult.rejectValue("file", "fileUploadError", e.getMessage());
            return "UploadPhotoView";
        } catch (IllegalStateException e) {
            model.addAttribute("error", e.getMessage());
            return "UploadPhotoView";
        } catch (UnauthorizedOperationException e) {
            model.addAttribute("error", e.getMessage());
            return "UnauthorizedView";
        }
        return "redirect:/contests/{contestId}";
    }

    @GetMapping("/{contestId}/contest-photo/{contestPhotoId}")
    public String delete(@PathVariable int contestPhotoId,
                         @PathVariable int contestId, Model model, HttpSession session) {
        User user;
        try {
            user = authenticationHelper.tryGetCurrentUser(session);
            contestPhotosService.delete(contestPhotoId, user);
        } catch (EntityNotFoundException e) {
            model.addAttribute("error", e.getMessage());
            return "NotFoundView";
        } catch (UnauthorizedOperationException e) {
            model.addAttribute("error", e.getMessage());
            return "UnauthorizedView";
        } catch (InvalidTimePeriodException e) {
            model.addAttribute("error", e.getMessage());
            return "InvalidOperation";
        }

        return "redirect:/contests/" + contestId;
    }

    @PostMapping(value = "/{contestId}/upload-cover-photo", consumes = {"multipart/form-data"})
    public String createCoverPhoto(@RequestParam("image") MultipartFile file,
                                   Model model,
                                   @PathVariable int contestId,
                                   HttpServletRequest request,
                                   HttpSession session) {

        model.addAttribute("requestURI", request.getRequestURI());

        try {
            User user = authenticationHelper.tryGetCurrentUser(session);
            Contest contest = contestService.getById(contestId);
            model.addAttribute("contest", contest);
            contestService.update(contest, user, file);


        } catch (EntityNotFoundException e) {
            model.addAttribute("error", e.getMessage());
            return "NotFoundView";
        } catch (AuthenticationFailureException e) {
            return "redirect:/auth/login";
        } catch (IllegalArgumentException | DuplicateEntityException | FileUploadException | IllegalStateException e) {
            model.addAttribute("error", e.getMessage());
            return "UploadPhotoView";
        } catch (UnauthorizedOperationException e) {
            model.addAttribute("error", e.getMessage());
            return "UnauthorizedView";
        }
        return "redirect:/contests";
    }

    @GetMapping("/{contestId}/contest-photos/{contestPhotoId}")
    public String showSingleContestPhoto(@Valid @PathVariable int contestId, @PathVariable int contestPhotoId,
                                         Model model, HttpSession session) {
        try {
            authenticationHelper.tryGetCurrentUser(session);
            ContestPhoto contestPhoto = contestPhotosService.get(contestPhotoId);
            model.addAttribute("contestPhoto", contestPhoto);
            List<User> jury = contestService.getContestJuryByContestId(contestId);
            model.addAttribute("jury", jury);


            Contest contest = contestService.getById(contestId);
            ContestOutDto contestOutDto = contestMapper.contestToOutDto(contest);

            List<PhotoReview> photoReviewList = photoReviewService.getAllByContestPhoto(contestPhoto);


            model.addAttribute("contest", contest);
            model.addAttribute("contestDto", contestOutDto);

            model.addAttribute("photoReviews", photoReviewList);


            if (contestOutDto.getCurrentPhase().equals("First phase")) {
                model.addAttribute("dueDate", contestMapper.contestToOutDto(contest).getPhaseTwoStart());
            } else if (contestOutDto.getCurrentPhase().equals("Second phase")) {
                model.addAttribute("dueDate", contestMapper.contestToOutDto(contest).getFinishedDate());
            }

            return "ContestPhotoView";
        } catch (EntityNotFoundException e) {
            model.addAttribute("error", e.getMessage());
            return "NotFoundView";
        } catch (UnauthorizedOperationException e) {
            model.addAttribute("error", e.getMessage());
            return "UnauthorizedView";
        }
    }
}