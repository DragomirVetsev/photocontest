package com.example.photocontest.controllers.rest;

import com.example.photocontest.exceptions.AuthenticationFailureException;
import com.example.photocontest.exceptions.EntityNotFoundException;
import com.example.photocontest.exceptions.UnauthorizedOperationException;
import com.example.photocontest.helpers.AuthenticationHelper;
import com.example.photocontest.helpers.mappers.ContestMapper;
import com.example.photocontest.models.Category;
import com.example.photocontest.models.Contest;
import com.example.photocontest.models.ContestFilterOptions;
import com.example.photocontest.models.User;
import com.example.photocontest.models.dto.ContestCreateDto;
import com.example.photocontest.models.dto.ContestOutDto;
import com.example.photocontest.models.dto.ContestUpdateDto;
import com.example.photocontest.services.contracts.CategoryService;
import com.example.photocontest.services.contracts.ContestService;
import jakarta.validation.Valid;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.server.ResponseStatusException;

import java.time.LocalDateTime;
import java.util.List;

@RestController
@RequestMapping("/api/contest")
public class ContestController {

    private final ContestService contestService;
    private final CategoryService categoryService;
    private final AuthenticationHelper authenticationHelper;
    private final ContestMapper contestMapper;


    @Autowired
    public ContestController(ContestService contestService, CategoryService categoryService, AuthenticationHelper authenticationHelper, ContestMapper contestMapper) {
        this.contestService = contestService;
        this.categoryService = categoryService;
        this.authenticationHelper = authenticationHelper;
        this.contestMapper = contestMapper;
    }

    @GetMapping("/{id}")
    public ContestOutDto getById(@PathVariable int id) {
        try {
            return contestMapper.contestToOutDto(contestService.getById(id));
        } catch (EntityNotFoundException e) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, e.getMessage());
        }
    }

    @GetMapping
    public List<ContestOutDto> get(
            @RequestParam(required = false) String title,
            @RequestParam(required = false) Integer categoryId,
            @RequestParam(required = false) String phase,
            @RequestParam(required = false) String sortBy,
            @RequestParam(required = false) String sortOrder) {
        ContestFilterOptions filterOptions = new ContestFilterOptions(title, categoryId, phase, sortBy, sortOrder);
        return contestMapper.contestsToOutDto(contestService.get(filterOptions));
    }

    @GetMapping("/categories")
    public List<Category> getAllCategories() {
        return categoryService.getAll();
    }

    @PostMapping("/categories/new")
    public Category create(@Valid @RequestBody Category category, @RequestHeader HttpHeaders headers) {
        try {
            User user = authenticationHelper.tryGetUser(headers);
            categoryService.create(category, user);
            return category;
        } catch (EntityNotFoundException e) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, e.getMessage());
        } catch (AuthenticationFailureException e) {
            throw new ResponseStatusException(HttpStatus.UNAUTHORIZED, e.getMessage());
        }
    }

    @PutMapping("/category/{id}")
    public Category update(@RequestHeader HttpHeaders headers, @PathVariable int id, @Valid @RequestBody Category category) {
        try {
            User user = authenticationHelper.tryGetUser(headers);
            categoryService.update(category, user);
            return category;
        } catch (EntityNotFoundException e) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, e.getMessage());
        } catch (UnauthorizedOperationException e) {
            throw new ResponseStatusException(HttpStatus.UNAUTHORIZED, e.getMessage());
        }
    }

    @DeleteMapping("/category/{id}")
    public void deleteCategory(@RequestHeader HttpHeaders headers, @PathVariable int id) {
        try {
            User user = authenticationHelper.tryGetUser(headers);
            categoryService.delete(id, user);
        } catch (EntityNotFoundException e) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, e.getMessage());
        } catch (UnauthorizedOperationException e) {
            throw new ResponseStatusException(HttpStatus.UNAUTHORIZED, e.getMessage());
        }
    }

    @PostMapping
    public ContestOutDto create(@Valid @RequestBody ContestCreateDto contestCreateDto, @RequestHeader HttpHeaders headers) {
        try {
            User user = authenticationHelper.tryGetUser(headers);
            Contest contest = contestMapper.fromDtoCreate(contestCreateDto, user);
            contestService.create(contest, user);
            return contestMapper.contestToOutDto(contest);
        } catch (EntityNotFoundException e) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, e.getMessage());
        } catch (AuthenticationFailureException e) {
            throw new ResponseStatusException(HttpStatus.UNAUTHORIZED, e.getMessage());
        }
    }

    @PutMapping("/{id}")
    public Contest update(@RequestHeader HttpHeaders headers, @PathVariable int id, @Valid @RequestBody ContestUpdateDto contestUpdateDto) {
        try {
            User user = authenticationHelper.tryGetUser(headers);
            Contest contest = contestMapper.fromDtoUpdate(id, contestUpdateDto);
            contestService.update(contest, user);
            return contest;
        } catch (EntityNotFoundException e) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, e.getMessage());
        } catch (UnauthorizedOperationException e) {
            throw new ResponseStatusException(HttpStatus.UNAUTHORIZED, e.getMessage());
        }
    }

    @DeleteMapping("/{id}")
    public void delete(@RequestHeader HttpHeaders headers, @PathVariable int id) {
        try {
            User user = authenticationHelper.tryGetUser(headers);
            contestService.delete(id, user);
        } catch (EntityNotFoundException e) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, e.getMessage());
        } catch (UnauthorizedOperationException e) {
            throw new ResponseStatusException(HttpStatus.UNAUTHORIZED, e.getMessage());
        }
    }

    @GetMapping("/phase-one")
    public List<Contest> getContestFirst(@RequestHeader HttpHeaders headers) {
        try {
            User user = authenticationHelper.tryGetUser(headers);
            return contestService.getContestPhaseOne(LocalDateTime.now(), user);
        } catch (UnauthorizedOperationException e) {
            throw new ResponseStatusException(HttpStatus.UNAUTHORIZED, e.getMessage());
        }
    }

    @GetMapping("/phaseTwo")
    public List<Contest> getContestSecond(@RequestHeader HttpHeaders headers) {
        try {
            User user = authenticationHelper.tryGetUser(headers);
            return contestService.getContestPhaseTwo(LocalDateTime.now(), user);
        } catch (UnauthorizedOperationException e) {
            throw new ResponseStatusException(HttpStatus.UNAUTHORIZED, e.getMessage());
        }
    }


    @GetMapping("/finished")
    public List<Contest> getContestFinished(@RequestHeader HttpHeaders headers) {
        try {
            User user = authenticationHelper.tryGetUser(headers);
            return contestService.getContestPhaseFinished(LocalDateTime.now(), user);
        } catch (UnauthorizedOperationException e) {
            throw new ResponseStatusException(HttpStatus.UNAUTHORIZED, e.getMessage());
        }

    }

}
