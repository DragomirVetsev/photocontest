package com.example.photocontest.controllers.mvc;

import com.example.photocontest.exceptions.EntityNotFoundException;
import com.example.photocontest.exceptions.InvalidInputOperation;
import com.example.photocontest.exceptions.UnauthorizedOperationException;
import com.example.photocontest.helpers.AuthenticationHelper;
import com.example.photocontest.models.Contest;
import com.example.photocontest.models.ContestPhoto;
import com.example.photocontest.models.ContestPhotoFilterOptions;
import com.example.photocontest.models.User;
import com.example.photocontest.models.dto.ContestPhotoFilterDto;
import com.example.photocontest.services.contracts.ContestPhotoService;
import com.example.photocontest.services.contracts.ContestService;
import jakarta.servlet.http.HttpSession;
import jakarta.validation.Valid;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;

@Controller
@RequestMapping("/contest-photos")
public class ContestPhotosMvcController {
    private final ContestPhotoService contestPhotosService;
    private final AuthenticationHelper authenticationHelper;
    private final ContestService contestService;

    @Autowired
    public ContestPhotosMvcController(ContestPhotoService contestPhotosService,
                                      AuthenticationHelper authenticationHelper, ContestService contestService) {
        this.contestPhotosService = contestPhotosService;
        this.authenticationHelper = authenticationHelper;
        this.contestService = contestService;
    }

    @GetMapping("/pick-cover/{contestId}")
    public String pickCoverPhotoForContest(@ModelAttribute("filterOptions") ContestPhotoFilterDto contestPhotoFilterDto,
                                           @Valid @PathVariable int contestId,
                                           Model model, HttpSession session) {

        ContestPhotoFilterOptions contestPhotoFilterOptions = new ContestPhotoFilterOptions(contestPhotoFilterDto.getTitle(),
                contestPhotoFilterDto.getSortBy(), contestPhotoFilterDto.getSortOrder());

        try {
            authenticationHelper.tryGetCurrentUser(session);
            model.addAttribute("contestId", contestId);
            model.addAttribute("filterOptions", contestPhotoFilterDto);
            model.addAttribute("contestPhotos", contestPhotosService.getAll(contestPhotoFilterOptions));

            return "PickCoverPhoto";
        } catch (UnauthorizedOperationException e) {
            return "UnauthorizedView";
        }
    }

    @GetMapping("/set-cover/{contestId}/{contestPhotoId}")
    public String pickCoverPhotoForContest(@Valid @PathVariable int contestId, @PathVariable int contestPhotoId,
                                           Model model,
                                           HttpSession session) {

        User user;
        try {
            user = authenticationHelper.tryGetCurrentUser(session);
        } catch (UnauthorizedOperationException e) {
            return "redirect:/auth/login";
        }

        try {
            Contest contest = contestService.getById(contestId);
            ContestPhoto contestPhoto = contestPhotosService.get(contestPhotoId);
            contest.setCoverPhotoName(contestPhoto.getPhotoUrl());
            contestService.update(contest, user);
            return "redirect:/contests";
        } catch (EntityNotFoundException e) {
            model.addAttribute("error", e.getMessage());
            return "NotFoundView";
        } catch (UnauthorizedOperationException e) {
            model.addAttribute("error", e.getMessage());
            return "UnauthorizedView";
        } catch (InvalidInputOperation e) {
            model.addAttribute("error", e.getMessage());
            return "redirect:/contests/{id}/update";
        }
    }
}
