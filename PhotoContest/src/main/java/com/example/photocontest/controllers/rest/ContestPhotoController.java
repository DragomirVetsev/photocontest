package com.example.photocontest.controllers.rest;

import com.example.photocontest.exceptions.*;
import com.example.photocontest.helpers.AuthenticationHelper;
import com.example.photocontest.helpers.mappers.ContestPhotoMapper;
import com.example.photocontest.models.ContestPhoto;
import com.example.photocontest.models.ContestPhotoFilterOptions;
import com.example.photocontest.models.User;
import com.example.photocontest.models.dto.ContestPhotoOutputDto;
import com.example.photocontest.services.contracts.ContestPhotoService;
import jakarta.validation.Valid;
import jakarta.validation.constraints.NotBlank;
import jakarta.validation.constraints.NotEmpty;
import jakarta.validation.constraints.NotNull;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.server.ResponseStatusException;

import java.util.List;

@RestController
@Valid
@RequestMapping("/api/contest-photo")
public class ContestPhotoController {

    private final ContestPhotoService contestPhotosService;
    private final ContestPhotoMapper contestPhotoMapper;
    private final AuthenticationHelper authenticationHelper;

    @Autowired
    public ContestPhotoController(ContestPhotoService contestPhotosService, ContestPhotoMapper contestPhotoMapper,
                                  AuthenticationHelper authenticationHelper) {
        this.contestPhotosService = contestPhotosService;
        this.contestPhotoMapper = contestPhotoMapper;
        this.authenticationHelper = authenticationHelper;
    }

    private static void throwIfEmpty(String field) {
        if (field.isEmpty()) {
            throw new ResponseStatusException(HttpStatus.BAD_REQUEST, "field cannot be empty");
        }
    }

    @GetMapping
    public List<ContestPhotoOutputDto> get(@RequestParam(required = false) String title,
                                           @RequestParam(required = false) String sortBy,
                                           @RequestParam(required = false) String sortOrder) {

        ContestPhotoFilterOptions contestPhotoFilterOptions = new ContestPhotoFilterOptions(title, sortBy, sortOrder);

        return contestPhotoMapper.toOutputListDto(contestPhotosService.getAll(contestPhotoFilterOptions));
    }

    @GetMapping("/{id}")
    public ContestPhotoOutputDto get(@PathVariable int id) {
        try {
            ContestPhoto contestPhoto = contestPhotosService.get(id);
            return contestPhotoMapper.toOutputDto(contestPhoto);
        } catch (EntityNotFoundException e) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, e.getMessage());
        }
    }

    @PostMapping
    @Validated
    public String handleFileUpload(@RequestHeader HttpHeaders httpHeaders,
                                   @Valid @NotEmpty @RequestParam(value = "file") MultipartFile file,
                                   @Valid @NotBlank @RequestParam("contestIdString") String contestIdString,
                                   @Valid @NotNull @RequestParam(name = "photoTitle") String photoTitle,
                                   @Valid @NotEmpty @RequestParam(name = "story") String story) {

        throwIfEmpty(contestIdString);
        throwIfEmpty(photoTitle);
        throwIfEmpty(story);

        ContestPhoto contestPhoto;
        User user;
        try {
            user = authenticationHelper.tryGetUser(httpHeaders);
            int contestId = Integer.parseInt(contestIdString);

            String fileName = file.getOriginalFilename();
            contestPhoto = contestPhotoMapper.contestPhotoToUpload(user, contestId, photoTitle, story, fileName);
            contestPhotosService.createContestPhoto(contestPhoto, file);

        } catch (EntityNotFoundException e) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, e.getMessage());
        } catch (AuthenticationFailureException e) {
            throw new ResponseStatusException(HttpStatus.UNAUTHORIZED, e.getMessage());
        } catch (IllegalArgumentException | DuplicateEntityException | FileUploadException e) {
            throw new ResponseStatusException(HttpStatus.BAD_REQUEST, e.getMessage());
        } catch (IllegalStateException e) {
            throw new ResponseStatusException(HttpStatus.FORBIDDEN, e.getMessage());
        }
        return "Success";
    }

    @DeleteMapping("/{contestPhotoId}")
    public String delete(@RequestHeader HttpHeaders headers, @PathVariable int contestPhotoId) {
        try {
            User user = authenticationHelper.tryGetUser(headers);
            contestPhotosService.delete(contestPhotoId, user);
        } catch (EntityNotFoundException e) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, e.getMessage());
        } catch (UnauthorizedOperationException e) {
            throw new ResponseStatusException(HttpStatus.UNAUTHORIZED, e.getMessage());
        }

        return "Success";
    }
}
