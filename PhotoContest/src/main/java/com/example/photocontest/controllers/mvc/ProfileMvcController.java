package com.example.photocontest.controllers.mvc;

import com.example.photocontest.exceptions.EntityNotFoundException;
import com.example.photocontest.exceptions.UnauthorizedOperationException;
import com.example.photocontest.helpers.AuthenticationHelper;
import com.example.photocontest.models.ContestPhoto;
import com.example.photocontest.models.User;
import com.example.photocontest.models.WinnerPhoto;
import com.example.photocontest.services.contracts.ContestPhotoService;
import com.example.photocontest.services.contracts.UserService;
import com.example.photocontest.services.contracts.WinnerPhotosService;
import jakarta.servlet.http.HttpSession;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;

import java.util.List;

@Controller
@RequestMapping("/profile")
public class ProfileMvcController {

    private final UserService userService;
    private final ContestPhotoService contestPhotoService;
    private final AuthenticationHelper authenticationHelper;
    private final WinnerPhotosService winnerPhotosService;

    public ProfileMvcController(UserService userService, ContestPhotoService contestPhotoService,
                                AuthenticationHelper authenticationHelper,
                                WinnerPhotosService winnerPhotosService) {
        this.userService = userService;
        this.contestPhotoService = contestPhotoService;
        this.winnerPhotosService = winnerPhotosService;
        this.authenticationHelper = authenticationHelper;
    }

    @ModelAttribute("isAuthenticated")
    public boolean populateIsAuthenticated(HttpSession session) {
        return session.getAttribute("currentUser") != null;
    }

    @ModelAttribute("currentUserName")
    public String populateCurrentUserName(HttpSession session) {
        return (String) session.getAttribute("currentUser");
    }

    @ModelAttribute("isOrganizer")
    public boolean isOrganizer(HttpSession session) {
        User user;
        try {
            user = userService.getUserByUsername(populateCurrentUserName(session));
            return userService.isOrganizer(user);
        } catch (EntityNotFoundException e) {
            return false;
        }
    }

    @ModelAttribute("user")
    public User populateUser(HttpSession session) {
        return userService.getUserByUsername(populateCurrentUserName(session));
    }

    @GetMapping
    public String getUserProfile(HttpSession session, Model model) {
        User user;
        try {
            user = authenticationHelper.tryGetCurrentUser(session);
        } catch (UnauthorizedOperationException | EntityNotFoundException e) {
            return "redirect:/auth/login";
        }
        List<ContestPhoto> userPhotos = contestPhotoService.getContestPhotosByUser(user);
        model.addAttribute("contestPhotos", userPhotos);

        List<WinnerPhoto> winnerPhotos = winnerPhotosService.getWinnerPhotosByUser(user);
        model.addAttribute("winnerPhotos", winnerPhotos);

        return "UserProfileView";
    }
}
