package com.example.photocontest.controllers.mvc;

import com.example.photocontest.exceptions.AuthenticationFailureException;
import com.example.photocontest.exceptions.DuplicateEntityException;
import com.example.photocontest.exceptions.EntityNotFoundException;
import com.example.photocontest.exceptions.UnauthorizedOperationException;
import com.example.photocontest.helpers.AuthenticationHelper;
import com.example.photocontest.helpers.mappers.UserMapper;
import com.example.photocontest.models.ContestPhoto;
import com.example.photocontest.models.User;
import com.example.photocontest.models.WinnerPhoto;
import com.example.photocontest.models.dto.UpdateUserDto;
import com.example.photocontest.services.contracts.ContestPhotoService;
import com.example.photocontest.services.contracts.UserService;
import com.example.photocontest.services.contracts.WinnerPhotosService;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpSession;
import jakarta.validation.Valid;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@Controller
@RequestMapping("/users")
public class UserMvcController {

    private final UserService userService;
    private final UserMapper userMapper;

    private final ContestPhotoService contestPhotoService;
    private final AuthenticationHelper authenticationHelper;

    private final WinnerPhotosService winnerPhotosService;

    @Autowired
    public UserMvcController(UserService userService, UserMapper userMapper, ContestPhotoService contestPhotoService, AuthenticationHelper authenticationHelper, WinnerPhotosService winnerPhotosService) {
        this.userService = userService;
        this.userMapper = userMapper;
        this.contestPhotoService = contestPhotoService;
        this.authenticationHelper = authenticationHelper;
        this.winnerPhotosService = winnerPhotosService;
    }


    @GetMapping("/{id}/promote-to-organizer")
    public String promoteToOrganizer(@Valid @PathVariable int id, HttpSession session, Model model) {
        try {
            User user = authenticationHelper.tryGetCurrentUser(session);
            userService.changeRoleOfUser(user, id);
            return "redirect:/panel";
        } catch (UnauthorizedOperationException e) {
            model.addAttribute("error", e.getMessage());
            return "UnauthorizedView";
        }
    }

    @GetMapping("/{id}/make-junkie")
    public String makePhotoJunkie(@Valid @PathVariable int id, HttpSession session, Model model) {
        try {
            User user = authenticationHelper.tryGetCurrentUser(session);
            userService.changeRoleOfUser(user, id);
            return "redirect:/panel";
        } catch (UnauthorizedOperationException e) {
            model.addAttribute("error", e.getMessage());
            return "UnauthorizedView";
        }
    }

    @GetMapping("/{id}/update")
    public String showEditUserPage(HttpServletRequest request, @PathVariable int id, Model model, HttpSession session) {
        try {
            authenticationHelper.tryGetCurrentUser(session);
        } catch (UnauthorizedOperationException e) {
            return "redirect:/auth/login";
        }
        try {
            User user = userService.get(id);
            UpdateUserDto updateUserDto = userMapper.toDtoUpdate(user);
            model.addAttribute("requestURI", request.getRequestURI());
            model.addAttribute("userId", user.getId());
            model.addAttribute("userUpdateDto", updateUserDto);
            return "UpdateInfoView";
        } catch (EntityNotFoundException e) {
            model.addAttribute("error", e.getMessage());
            return "NotFoundView";
        }
    }

    @PostMapping("/{id}/update")
    public String userUpdate(@Valid @PathVariable int id,
                             @ModelAttribute("userUpdateDto") UpdateUserDto updateUserDto,
                             BindingResult bindingResult,
                             HttpServletRequest request,
                             Model model,
                             HttpSession session) {
        User user;
        try {
            authenticationHelper.tryGetCurrentUser(session);
        } catch (UnauthorizedOperationException e) {
            return "redirect:/auth/login";
        }
        if (bindingResult.hasErrors()) {
            return "UpdateInfoView";
        }
        model.addAttribute("requestURI", request.getRequestURI());
        try {
            user = userMapper.fromDtoUpdate(id, updateUserDto);
            userService.update(id, user);
            return "redirect:/profile";
        } catch (EntityNotFoundException e) {
            model.addAttribute("error", e.getMessage());
            return "NotFoundView";
        } catch (UnauthorizedOperationException e) {
            model.addAttribute("error", e.getMessage());
            return "UnauthorizedView";
        } catch (AuthenticationFailureException | DuplicateEntityException e) {
            model.addAttribute("error", e.getMessage());
            return "UpdateInfoView";
        }
    }

    @GetMapping("/{id}/delete")
    public String deleteUser(@PathVariable int id, Model model, HttpSession session) {
        User user;
        try {
            user = authenticationHelper.tryGetCurrentUser(session);
        } catch (UnauthorizedOperationException e) {
            return "redirect:/auth/login";
        }
        try {
            if (user.getId() != id) {
                userService.delete(id, user);
                return "redirect:/panel";
            } else {
                userService.delete(id, user);
                session.removeAttribute("currentUser");
                return "redirect:/";
            }
        } catch (EntityNotFoundException e) {
            model.addAttribute("error", e.getMessage());
            return "NotFoundView";
        } catch (UnauthorizedOperationException e) {
            model.addAttribute("error", e.getMessage());
            return "UnauthorizedView";
        }
    }

    @GetMapping("/{id}/pictures")
    public String getUserPictures(@PathVariable int id, HttpSession session, Model model) {
        try {
            authenticationHelper.tryGetCurrentUser(session);
        } catch (UnauthorizedOperationException | EntityNotFoundException e) {
            return "redirect:/auth/login";
        }

        User userToGet = userService.get(id);
        model.addAttribute("user", userToGet);
        List<ContestPhoto> userPhotos = contestPhotoService.getContestPhotosByUser(userToGet);
        model.addAttribute("contestPhotos", userPhotos);

        List<WinnerPhoto> winnerPhotos = winnerPhotosService.getWinnerPhotosByUser(userToGet);
        model.addAttribute("winnerPhotos", winnerPhotos);

        return "UserPicturesView";
    }
}