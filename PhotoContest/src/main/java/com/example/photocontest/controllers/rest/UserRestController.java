package com.example.photocontest.controllers.rest;

import com.example.photocontest.exceptions.AuthenticationFailureException;
import com.example.photocontest.exceptions.DuplicateEntityException;
import com.example.photocontest.exceptions.EntityNotFoundException;
import com.example.photocontest.exceptions.UnauthorizedOperationException;
import com.example.photocontest.helpers.AuthenticationHelper;
import com.example.photocontest.helpers.mappers.UserMapper;
import com.example.photocontest.models.User;
import com.example.photocontest.models.UserFilterOptions;
import com.example.photocontest.models.dto.RegisterDto;
import com.example.photocontest.models.dto.UserRequestDto;
import com.example.photocontest.models.dto.UserResponseDto;
import com.example.photocontest.services.contracts.UserService;
import jakarta.validation.Valid;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.server.ResponseStatusException;

import java.util.List;

@RestController
@RequestMapping("/api/users")
public class UserRestController {

    public static final String ERROR_MESSAGE = "You are not authorized to perform this action.";

    private final UserService userService;

    private final UserMapper userMapper;

    private final AuthenticationHelper authenticationHelper;

    @Autowired
    public UserRestController(UserService userService, UserMapper userMapper,
                              AuthenticationHelper authenticationHelper) {
        this.userService = userService;
        this.userMapper = userMapper;
        this.authenticationHelper = authenticationHelper;
    }

    @GetMapping("/filter")
    public List<UserResponseDto> filter(
            @RequestParam(required = false) String username,
            @RequestParam(required = false) String firstName,
            @RequestParam(required = false) String lastName,
            @RequestParam(required = false) String sortBy,
            @RequestParam(required = false) String sortOrder) {
        UserFilterOptions userFilterOptions = new UserFilterOptions(username, firstName, lastName, sortBy, sortOrder);
        return userMapper.toResponseDto(userService.get(userFilterOptions));
    }

    @GetMapping
    public List<UserResponseDto> get(@RequestHeader HttpHeaders headers) {
        try {
            User user = authenticationHelper.tryGetUser(headers);
            if (!userService.isOrganizer(user)) {
                throw new ResponseStatusException(HttpStatus.UNAUTHORIZED, ERROR_MESSAGE);
            }
            return userMapper.toResponseDto(userService.getAll());
        } catch (AuthenticationFailureException e) {
            throw new ResponseStatusException(HttpStatus.UNAUTHORIZED, e.getMessage());
        }
    }

    @GetMapping("/{id}")
    public UserResponseDto get(@RequestHeader HttpHeaders headers, @PathVariable Integer id) {
        try {
            User user = authenticationHelper.tryGetUser(headers);
            checkAccessPermissions(id, user);
            return userMapper.toResponseDto(userService.get(id));
        } catch (EntityNotFoundException e) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, e.getMessage());
        } catch (AuthenticationFailureException e) {
            throw new ResponseStatusException(HttpStatus.UNAUTHORIZED, e.getMessage());
        } catch (UnauthorizedOperationException e) {
            throw new ResponseStatusException(HttpStatus.FORBIDDEN, e.getMessage());
        }
    }

    @PostMapping
    public UserResponseDto create(@Valid @RequestBody RegisterDto registerDto) {
        try {
            User user = userMapper.fromRegisterDto(registerDto);
            userService.create(user);
            return userMapper.toResponseDto(user);
        } catch (EntityNotFoundException e) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, e.getMessage());
        } catch (DuplicateEntityException e) {
            throw new ResponseStatusException(HttpStatus.CONFLICT, e.getMessage());
        } catch (AuthenticationFailureException e) {
            throw new ResponseStatusException(HttpStatus.UNAUTHORIZED, e.getMessage());
        }
    }

    @PutMapping("/{id}")
    public UserResponseDto update(@RequestHeader HttpHeaders headers, @PathVariable Integer id,
                                  @Valid @RequestBody UserRequestDto userRequestDto) {
        try {
            User executingUser = authenticationHelper.tryGetUser(headers);
            checkAccessPermissions(id, executingUser);
            User userToUpdate = userMapper.fromDto(id, userRequestDto);
            userService.update(id, userToUpdate);
            return userMapper.toResponseDto(userToUpdate);
        } catch (EntityNotFoundException e) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, e.getMessage());
        } catch (DuplicateEntityException e) {
            throw new ResponseStatusException(HttpStatus.CONFLICT, e.getMessage());
        } catch (AuthenticationFailureException e) {
            throw new ResponseStatusException(HttpStatus.UNAUTHORIZED, e.getMessage());
        } catch (UnauthorizedOperationException e) {
            throw new ResponseStatusException(HttpStatus.FORBIDDEN, e.getMessage());
        }
    }

    @DeleteMapping("/{id}")
    public void delete(@RequestHeader HttpHeaders headers, @PathVariable Integer id) {
        try {
            User executingUser = authenticationHelper.tryGetUser(headers);
            checkAccessPermissions(id, executingUser);
            User userToDelete = userService.get(id);
            userService.delete(id, userToDelete);
        } catch (EntityNotFoundException e) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, e.getMessage());
        } catch (AuthenticationFailureException e) {
            throw new ResponseStatusException(HttpStatus.UNAUTHORIZED, e.getMessage());
        } catch (UnauthorizedOperationException e) {
            throw new ResponseStatusException(HttpStatus.FORBIDDEN, e.getMessage());
        }
    }

    private void checkAccessPermissions(Integer targetUserId, User executingUser) {
        if (executingUser.getId() != targetUserId) {
            throw new UnauthorizedOperationException(ERROR_MESSAGE);
        }
    }
}
