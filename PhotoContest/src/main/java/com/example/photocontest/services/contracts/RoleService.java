package com.example.photocontest.services.contracts;

import com.example.photocontest.models.Role;

import java.util.List;

public interface RoleService {

    List<Role> getAll();

    Role get(int id);

    Role get(String name);
}
