package com.example.photocontest.services;

import com.example.photocontest.models.Role;
import com.example.photocontest.repositories.contracts.RoleRepository;
import com.example.photocontest.services.contracts.RoleService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class RoleServiceImpl implements RoleService {

    private final RoleRepository roleRepository;

    @Autowired
    public RoleServiceImpl(RoleRepository roleRepository) {
        this.roleRepository = roleRepository;
    }

    @Override
    public List<Role> getAll() {
        return roleRepository.getAll();
    }

    @Override
    public Role get(int id) {
        return roleRepository.get(id);
    }

    @Override
    public Role get(String name) {
        return roleRepository.get(name);
    }
}
