package com.example.photocontest.services.contracts;

import com.example.photocontest.models.Rank;
import com.example.photocontest.models.User;

import java.util.List;

public interface RankService {
    List<Rank> getAll();

    Rank get(int id);

    Rank get(String name);

    Rank getRankByScore(int score);

    void assignRank(int score, User user);
}
