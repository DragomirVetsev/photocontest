package com.example.photocontest.services.contracts;

import com.example.photocontest.models.ContestPhoto;
import com.example.photocontest.models.ContestPhotoFilterOptions;
import com.example.photocontest.models.User;
import com.example.photocontest.models.WinnerPhoto;
import org.springframework.web.multipart.MultipartFile;

import java.util.List;

public interface ContestPhotoService {

    List<ContestPhoto> getAll(ContestPhotoFilterOptions contestPhotoFilterOptions);

    ContestPhoto get(int id);

    List<ContestPhoto> getContestPhotosByContestId(int contestId);

    void delete(int contestPhotoId, User user);

    void createContestPhoto(ContestPhoto contestPhoto, MultipartFile file);

    List<WinnerPhoto> getAllWinners();

    void setUserPointsForParticipation(User user, int contestId);

    List<ContestPhoto> getContestPhotosByUser(User user);
}
