package com.example.photocontest.services;

import com.example.photocontest.exceptions.EntityNotFoundException;
import com.example.photocontest.exceptions.InvalidTimePeriodException;
import com.example.photocontest.helpers.mappers.WinnerPhotoMapper;
import com.example.photocontest.models.*;
import com.example.photocontest.models.dto.ContestPhotoAndPointsDto;
import com.example.photocontest.repositories.contracts.*;
import com.example.photocontest.services.contracts.WinnerPhotosService;
import jakarta.persistence.EntityExistsException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;

@Service
public class WinnerPhotosServiceImpl implements WinnerPhotosService {
    public static final int FIRST_PLACE_WITH_DOUBLE_THE_SECOND_PLACE_POINTS = 75;
    public static final int SHARED_FIRST_PLACE_RANG_POINTS = 40;
    public static final int SINGLE_FIRST_PLACE_RANG_POINTS = 50;
    public static final int SINGLE_SECOND_PLACE_RANG_POINTS = 35;
    public static final String NO_REVIEWS_FOR_THIS_CONTEST = "No reviews for this contest";
    public static final String CANNOT_CREATE_WINNERS_BEFORE_END_OF_PHASE_2 = "Cannot create winners before end of phase 2";
    private static final int SHARED_SECOND_PLACE_RANG_POINTS = 25;
    private static final int SHARED_THIRD_PLACE_RANG_POINTS = 10;
    private static final int SINGLE_THIRD_PLACE_RANG_POINTS = 20;
    WinnerPhotosRepository winnerPhotosRepository;
    PhotoReviewRepository photoReviewRepository;
    ContestRepository contestRepository;
    PlaceRepository placeRepository;
    UserRepository userRepository;
    RankRepository rankRepository;

    ContestPhotosRepository contestPhotosRepository;
    WinnerPhotoMapper winnerPhotoMapper;

    @Autowired
    public WinnerPhotosServiceImpl(WinnerPhotosRepository winnerPhotosRepository, ContestRepository contestRepository,
                                   PhotoReviewRepository photoReviewRepository, PlaceRepository placeRepository,
                                   WinnerPhotoMapper winnerPhotoMapper,
                                   UserRepository userRepository, RankRepository rankRepository,
                                   ContestPhotosRepository contestPhotosRepository) {
        this.contestRepository = contestRepository;
        this.photoReviewRepository = photoReviewRepository;
        this.winnerPhotosRepository = winnerPhotosRepository;
        this.placeRepository = placeRepository;
        this.winnerPhotoMapper = winnerPhotoMapper;
        this.userRepository = userRepository;
        this.rankRepository = rankRepository;
        this.contestPhotosRepository = contestPhotosRepository;
    }

    private static void removeBlockedPhotos(List<ContestPhoto> contestPhotoList, List<ContestPhoto> contestBlockedPhotos) {
        for (ContestPhoto blockedPhoto : contestBlockedPhotos) {
            contestPhotoList.remove(blockedPhoto);
        }
    }

    @Override
    public List<WinnerPhoto> getAllWinners() {
        List<Contest> finishedContestsWithoutWinners = contestRepository.getFinishedContestsWithoutWinners();
        for (Contest contest : finishedContestsWithoutWinners) {
            createWinners(contest.getId());
        }
        return winnerPhotosRepository.getAll();
    }

    @Override
    public void createWinners(int contestId) {
        throwIfPhaseTwoNotFinished(contestId);
        throwIfWinnersAlreadyGenerated(contestId);

        Contest contest = contestRepository.getById(contestId);
        givePointsToUnreviewedContestPhotos(contest);

        List<ContestPhotoAndPointsDto> contestPhotoAndPointsList = photoReviewRepository.getDtosByContest(contest);
        if (contestPhotoAndPointsList.isEmpty()) {
            throw new EntityNotFoundException(NO_REVIEWS_FOR_THIS_CONTEST + contest.getTitle());
        }
        List<ContestPhotoAndPointsDto> firstPlaces = new ArrayList<>();
        List<ContestPhotoAndPointsDto> secondPlaces = new ArrayList<>();
        List<ContestPhotoAndPointsDto> thirdPlaces = new ArrayList<>();

        for (ContestPhotoAndPointsDto contestPhotoAndPointsDto : contestPhotoAndPointsList) {
            if (firstPlaces.isEmpty()) {
                firstPlaces.add(contestPhotoAndPointsDto);
            } else if (firstPlaces.get(0).getScore() == contestPhotoAndPointsDto.getScore()) {
                firstPlaces.add(contestPhotoAndPointsDto);
            } else if (secondPlaces.isEmpty()) {
                secondPlaces.add(contestPhotoAndPointsDto);
            } else if (secondPlaces.get(0).getScore() == contestPhotoAndPointsDto.getScore()) {
                secondPlaces.add(contestPhotoAndPointsDto);
            } else if (thirdPlaces.isEmpty()) {
                thirdPlaces.add(contestPhotoAndPointsDto);
            } else if (thirdPlaces.get(0).getScore() == contestPhotoAndPointsDto.getScore()) {
                thirdPlaces.add(contestPhotoAndPointsDto);
            } else {
                break;
            }
        }
        createWinnerPhotos(firstPlaces, secondPlaces, thirdPlaces);
    }

    private void throwIfWinnersAlreadyGenerated(int contestId) {
        if (!winnerPhotosRepository.getContestPhotosWinnersByContestId(contestId).isEmpty()) {
            throw new EntityExistsException("Already have winners for contest with Id: " + contestId);
        }
    }

    public void createWinnerPhotos(List<ContestPhotoAndPointsDto> firstPlaces, List<ContestPhotoAndPointsDto> secondPlaces, List<ContestPhotoAndPointsDto> thirdPlaces) {
        int FirstPlaceRangPoints, secondPlaceRangPoints, thirdPlaceRangPoints;

        if (firstPlaces.size() == 1 && (secondPlaces.isEmpty() || firstPlaces.get(0).getScore() > secondPlaces.get(0).getScore() * 2)) {
            FirstPlaceRangPoints = FIRST_PLACE_WITH_DOUBLE_THE_SECOND_PLACE_POINTS;
        } else {
            if (firstPlaces.size() > 1) {
                FirstPlaceRangPoints = SHARED_FIRST_PLACE_RANG_POINTS;
            } else {
                FirstPlaceRangPoints = SINGLE_FIRST_PLACE_RANG_POINTS;
            }
        }
        updateWinnerPhotoTable(firstPlaces, FirstPlaceRangPoints, placeRepository.getById(1));
        updateUserRang(firstPlaces, FirstPlaceRangPoints);

        if (secondPlaces.size() > 1) {
            secondPlaceRangPoints = SHARED_SECOND_PLACE_RANG_POINTS;
        } else {
            secondPlaceRangPoints = SINGLE_SECOND_PLACE_RANG_POINTS;
        }
        updateWinnerPhotoTable(secondPlaces, secondPlaceRangPoints, placeRepository.getById(2));
        updateUserRang(secondPlaces, secondPlaceRangPoints);

        if (thirdPlaces.size() > 1) {
            thirdPlaceRangPoints = SHARED_THIRD_PLACE_RANG_POINTS;
        } else {
            thirdPlaceRangPoints = SINGLE_THIRD_PLACE_RANG_POINTS;
        }
        updateWinnerPhotoTable(thirdPlaces, thirdPlaceRangPoints, placeRepository.getById(3));
        updateUserRang(thirdPlaces, thirdPlaceRangPoints);
    }

    @Override
    public List<WinnerPhoto> getWinnerPhotosByUser(User user) {
        return winnerPhotosRepository.getByUser(user);
    }

    @Override
    public List<WinnerPhoto> getWinnerPhotosByContest(Contest contest) {
        List<WinnerPhoto> winnerPhotoList = winnerPhotosRepository.getWinnerPhotosByContest(contest);
        if (winnerPhotoList.isEmpty() && contest.getPhaseTwoEnd().isBefore(LocalDateTime.now())) {
            createWinners(contest.getId());
            winnerPhotoList = winnerPhotosRepository.getWinnerPhotosByContest(contest);
        }
        return winnerPhotoList;
    }

    @Override
    public void givePointsToUnreviewedContestPhotos(Contest contest) {
        throwIfPhaseTwoNotFinished(contest.getId());

        List<ContestPhoto> contestPhotoList = contestPhotosRepository.getContestPhotosByContestId(contest.getId());
        List<ContestPhoto> contestBlockedPhotos = photoReviewRepository.getBlockedPhotos(contest);
        removeBlockedPhotos(contestPhotoList, contestBlockedPhotos);
        List<User> jurors = getJurors(contest);
        createDefaultReviews(contest, contestPhotoList, jurors);
    }

    private void createDefaultReviews(Contest contest, List<ContestPhoto> contestPhotoList, List<User> jurors) {
        for (User juro : jurors) {
            List<ContestPhoto> juroredPhotos = photoReviewRepository.getAllByJuro(contest, juro);

            for (ContestPhoto contestPhoto : contestPhotoList) {
                if (!juroredPhotos.contains(contestPhoto)) {
                    PhotoReview photoReview = new PhotoReview();
                    photoReview.setContestPhoto(contestPhoto);
                    photoReview.setJuryMember(juro);
                    photoReview.setComment("Default score");
                    photoReview.setWrongCategory(false);
                    photoReview.setScore(3);
                    photoReview.setContest(contest);
                    photoReviewRepository.create(photoReview);
                }
            }
        }
    }

    private List<User> getJurors(Contest contest) {
        List<User> jurors = photoReviewRepository.getByContest(contest).stream()
                .map(PhotoReview::getJuryMember)
                .distinct()
                .toList();
        return jurors;
    }

    private void updateUserRang(List<ContestPhotoAndPointsDto> contestPhotosAndRangPoints, int rangPoints) {
        for (ContestPhotoAndPointsDto dto : contestPhotosAndRangPoints) {
            User user = dto.getContestPhoto().getUser();
            int newScore = user.getScore() + rangPoints;
            userRepository.updateUserRangAndScoreInDB(user, newScore);
        }
    }

    private void updateWinnerPhotoTable(List<ContestPhotoAndPointsDto> contestPhotosAndRangPoints, int rangPoints, Place place) {
        for (ContestPhotoAndPointsDto contestPhotoAndPointsDto : contestPhotosAndRangPoints) {
            WinnerPhoto winnerPhoto = winnerPhotoMapper.toWinnerPhoto(contestPhotoAndPointsDto, rangPoints, place);
            winnerPhotosRepository.create(winnerPhoto);
        }
    }

    private void throwIfPhaseTwoNotFinished(int contestId) {
        LocalDateTime currentDateTime = LocalDateTime.now();
        if (contestRepository.getById(contestId).getPhaseTwoEnd().isAfter(currentDateTime)) {
            throw new InvalidTimePeriodException(CANNOT_CREATE_WINNERS_BEFORE_END_OF_PHASE_2);
        }
    }
}
