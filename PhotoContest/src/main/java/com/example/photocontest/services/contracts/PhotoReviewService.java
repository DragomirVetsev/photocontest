package com.example.photocontest.services.contracts;

import com.example.photocontest.models.Contest;
import com.example.photocontest.models.ContestPhoto;
import com.example.photocontest.models.PhotoReview;
import com.example.photocontest.models.User;

import java.util.List;

public interface PhotoReviewService {
    List<PhotoReview> getAllByContestPhoto(ContestPhoto contestPhoto);

    PhotoReview get(int id, ContestPhoto contestPhoto);

    void create(PhotoReview photoReview, ContestPhoto contestPhoto, User juror);

    void update(PhotoReview photoReview);

    void delete(int id);

    List<PhotoReview> getAllByContest(Contest contest);
}
