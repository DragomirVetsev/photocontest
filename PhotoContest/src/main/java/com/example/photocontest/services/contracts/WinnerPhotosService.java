package com.example.photocontest.services.contracts;

import com.example.photocontest.models.Contest;
import com.example.photocontest.models.User;
import com.example.photocontest.models.WinnerPhoto;
import com.example.photocontest.models.dto.ContestPhotoAndPointsDto;

import java.util.List;

public interface WinnerPhotosService {
    List<WinnerPhoto> getAllWinners();

    void createWinners(int id);

    void createWinnerPhotos(List<ContestPhotoAndPointsDto> firstPlaces, List<ContestPhotoAndPointsDto> secondPlaces, List<ContestPhotoAndPointsDto> thirdPlaces);

    List<WinnerPhoto> getWinnerPhotosByUser(User user);

    List<WinnerPhoto> getWinnerPhotosByContest(Contest contest);

    void givePointsToUnreviewedContestPhotos(Contest contest);
}
