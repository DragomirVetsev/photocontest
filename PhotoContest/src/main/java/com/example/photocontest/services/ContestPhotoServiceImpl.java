package com.example.photocontest.services;

import com.example.photocontest.exceptions.DuplicateEntityException;
import com.example.photocontest.exceptions.FileUploadException;
import com.example.photocontest.exceptions.InvalidTimePeriodException;
import com.example.photocontest.exceptions.UnauthorizedOperationException;
import com.example.photocontest.models.*;
import com.example.photocontest.repositories.contracts.ContestPhotosRepository;
import com.example.photocontest.repositories.contracts.ContestRepository;
import com.example.photocontest.repositories.contracts.PhotoReviewRepository;
import com.example.photocontest.repositories.contracts.UserRepository;
import com.example.photocontest.services.contracts.ContestPhotoService;
import com.example.photocontest.services.contracts.ImageFilesOperations;
import com.example.photocontest.utils.FileConstants;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

import java.io.IOException;
import java.time.LocalDateTime;
import java.util.List;
import java.util.UUID;

import static com.example.photocontest.utils.UserRoleConstants.ORGANIZER;

@Service
public class ContestPhotoServiceImpl implements ContestPhotoService {

    public static final int RULE_VIOLATION_PENALTY = 10;
    public static final String ONLY_ORGANIZERS_CAN_DELETE_IMAGES = "Only organizers can delete images";
    public static final String CANNOT_DELETE_PHOTOS_AFTER_CONTEST_END = "Cannot delete photos after contest end";
    private static final String JURIES_AND_ORGANISERS_CANNOT_PARTICIPATE = "Juries and organisers cannot participate";
    private static final String YOU_CAN_UPLOAD_ONLY_AT_STATE_ONE = "You can upload only at state one";
    private static final int JOINING_NON_INVITATIONAL_CONTEST_SCORE = 1;
    private static final int JOINING_INVITATIONAL_CONTEST_SCORE = 3;
    private final ContestPhotosRepository contestPhotoRepository;
    private final UserRepository userRepository;
    private final ContestRepository contestRepository;
    private final ImageFilesOperations imageFileOperations;
    private final PhotoReviewRepository photoReviewRepository;


    @Autowired
    public ContestPhotoServiceImpl(ContestPhotosRepository contestPhotoRepository,
                                   UserRepository userRepository,
                                   ContestRepository contestRepository,
                                   ImageFilesOperations imageFileOperations,
                                   PhotoReviewRepository photoReviewRepository) {
        this.contestPhotoRepository = contestPhotoRepository;
        this.userRepository = userRepository;
        this.contestRepository = contestRepository;
        this.imageFileOperations = imageFileOperations;
        this.photoReviewRepository = photoReviewRepository;
    }

    private static void throwIfInvitationalAndNotInvited(User user, Contest contest) {
        if (contest.isInvitational() && (!contest.getInvitedUsers().contains(user))) {
            throw new UnauthorizedOperationException("Only invited people may join contest: " + contest.getTitle());
        }
    }

    private static void throwIfNotInPhaseOne(Contest contest) {
        if (!contest.isFirstPhase()) {
            throw new IllegalStateException(YOU_CAN_UPLOAD_ONLY_AT_STATE_ONE);
        }
    }

    private static void throwIfJuryOrOrganizer(User user, Contest contest) {
        if (user.getRole().getName().equals(ORGANIZER) || contest.getJuryMembers().contains(user)) {
            throw new UnauthorizedOperationException(JURIES_AND_ORGANISERS_CANNOT_PARTICIPATE);
        }
    }

    private static int getNewScore(User photoOwner) {
        int newScore = photoOwner.getScore() - RULE_VIOLATION_PENALTY;
        if (newScore < 0) {
            newScore = 0;
        }
        return newScore;
    }

    private static int getNewScore(User user, int pointsToAdd) {
        int oldScore = user.getScore();
        return oldScore + pointsToAdd;
    }

    @Override
    public List<ContestPhoto> getAll(ContestPhotoFilterOptions contestPhotoFilterOptions) {
        return contestPhotoRepository.getAllFilterAndSort(contestPhotoFilterOptions);
    }

    @Override
    public ContestPhoto get(int id) {
        return contestPhotoRepository.getById(id);
    }

    public List<ContestPhoto> getContestPhotosByContestId(int contestId) {
        return contestPhotoRepository.getContestPhotosByContestId(contestId);
    }

    public ContestPhoto createContestPhotoObj(ContestPhoto contestPhotoToCreate) {

        User user = contestPhotoToCreate.getUser();
        Contest contest = contestPhotoToCreate.getContest();
        throwIfDuplicateExist(contestPhotoToCreate);
        throwIfNotInPhaseOne(contest);
        throwIfJuryOrOrganizer(user, contest);
        throwIfInvitationalAndNotInvited(user, contest);

        String imageName = UUID.randomUUID() + contestPhotoToCreate.getPhotoUrl();
        contestPhotoToCreate.setPhotoUrl(FileConstants.PATH_TO_FILE + imageName);

        contestPhotoToCreate.setCreatedAt(LocalDateTime.now());

        return contestPhotoRepository.createAndReturnContestPhoto(contestPhotoToCreate);
    }

    private void throwIfDuplicateExist(ContestPhoto contestPhotoToCreate) {
        List<ContestPhoto> contestPhotos = contestPhotoRepository.getByUserAndContestIds(contestPhotoToCreate.getUser().getId(),
                contestPhotoToCreate.getContest().getId());

        if (!contestPhotos.isEmpty()) {
            throw new DuplicateEntityException("Already uploaded image for contest: " + contestPhotoToCreate.getContest().getTitle());
        }
    }

    @Override
    public void delete(int contestPhotoId, User user) {
        ContestPhoto contestPhoto = contestPhotoRepository.getById(contestPhotoId);
        Contest contest = contestPhoto.getContest();
        if (contest.getPhaseTwoEnd().isBefore(LocalDateTime.now())) {
            throw new InvalidTimePeriodException(CANNOT_DELETE_PHOTOS_AFTER_CONTEST_END);
        }
        if (!user.getRole().getName().equals(ORGANIZER)) {
            throw new UnauthorizedOperationException(ONLY_ORGANIZERS_CAN_DELETE_IMAGES);
        }
        imageFileOperations.deleteImage(contestPhoto.getPhotoUrl());
        photoReviewRepository.deleteByContestPhotoId(contestPhotoId);

        User photoOwner = contestPhoto.getUser();
        userRepository.updateUserRangAndScoreInDB(photoOwner, getNewScore(photoOwner));

        contestPhotoRepository.delete(contestPhotoId);
    }

    @Override
    public void createContestPhoto(ContestPhoto contestPhoto, MultipartFile file) {
        createContestPhotoObj(contestPhoto);
        try {
            imageFileOperations.uploadImage(file, contestPhoto.getPhotoUrl());
        } catch (IOException | IllegalArgumentException e) {
            contestPhotoRepository.delete(contestPhoto.getId());
            throw new FileUploadException(e.getMessage());
        }
    }

    @Override
    public List<WinnerPhoto> getAllWinners() {
        return contestPhotoRepository.getAllWinners();
    }

    @Override
    public void setUserPointsForParticipation(User user, int contestId) {
        int pointsToAdd = JOINING_NON_INVITATIONAL_CONTEST_SCORE;
        if (contestRepository.getById(contestId).isInvitational()) {
            pointsToAdd = JOINING_INVITATIONAL_CONTEST_SCORE;
        }
        userRepository.updateUserRangAndScoreInDB(user, getNewScore(user, pointsToAdd));
    }

    @Override
    public List<ContestPhoto> getContestPhotosByUser(User user) {
        return contestPhotoRepository.getContestPhotosByUser(user);
    }

}
