package com.example.photocontest.services;

import com.example.photocontest.models.Rank;
import com.example.photocontest.models.User;
import com.example.photocontest.repositories.contracts.RankRepository;
import com.example.photocontest.services.contracts.RankService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class RankServiceImpl implements RankService {

    private final RankRepository rankRepository;

    @Autowired
    public RankServiceImpl(RankRepository rankRepository) {
        this.rankRepository = rankRepository;
    }

    @Override
    public List<Rank> getAll() {
        return rankRepository.getAll();
    }

    @Override
    public Rank get(int id) {
        return rankRepository.get(id);
    }

    @Override
    public Rank get(String name) {
        return rankRepository.get(name);
    }

    @Override
    public void assignRank(int score, User user) {
        Rank rank = rankRepository.getRankByScore(score);
        user.setRank(rank);
    }

    @Override
    public Rank getRankByScore(int score) {
        return rankRepository.getRankByScore(score);
    }
}
