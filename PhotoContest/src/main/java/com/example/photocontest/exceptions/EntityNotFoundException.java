package com.example.photocontest.exceptions;

public class EntityNotFoundException extends RuntimeException {


    public EntityNotFoundException(String type, int id) {
        this(type, "id", String.valueOf(id));
    }

    public EntityNotFoundException(String type, String attribute, String value) {
        super(String.format("%s with %s %s not found.", type, attribute, value));
    }

    public EntityNotFoundException(String type, String attribute, int value) {
        super(String.format("%s with %s %d not found.", type, attribute, value));
    }

    public EntityNotFoundException(String errorMessage) {
        super(errorMessage);
    }
}
