package com.example.photocontest.repositories.contracts;

import com.example.photocontest.models.Contest;
import com.example.photocontest.models.ContestPhoto;
import com.example.photocontest.models.User;
import com.example.photocontest.models.WinnerPhoto;

import java.util.List;

public interface WinnerPhotosRepository extends BaseCRUDRepository<WinnerPhoto> {
    List<ContestPhoto> getContestPhotosWinnersByContestId(int contestId);

    List<WinnerPhoto> getByUser(User user);

    List<WinnerPhoto> getWinnerPhotosByContest(Contest contest);
}
