package com.example.photocontest.repositories;

import com.example.photocontest.models.ContestPhoto;
import com.example.photocontest.models.ContestPhotoFilterOptions;
import com.example.photocontest.models.User;
import com.example.photocontest.models.WinnerPhoto;
import com.example.photocontest.repositories.contracts.ContestPhotosRepository;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.query.Query;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Repository
public class ContestPhotoRepositoryImpl extends AbstractCRUDRepository<ContestPhoto> implements ContestPhotosRepository {

    SessionFactory sessionFactory;

    @Autowired
    public ContestPhotoRepositoryImpl(SessionFactory sessionFactory) {
        super(ContestPhoto.class, sessionFactory);
        this.sessionFactory = sessionFactory;
    }

    @Override
    public List<ContestPhoto> getByUserAndContestIds(int userId, int contestId) {
        try (Session session = sessionFactory.openSession()) {
            Query<ContestPhoto> query = session.createQuery("SELECT cp FROM ContestPhoto cp " +
                    "JOIN cp.contest c WHERE c.id = :contestId AND cp.user.id = :userId", ContestPhoto.class);
            query.setParameter("contestId", contestId);
            query.setParameter("userId", userId);

            return query.getResultList();
        }
    }

    @Override
    public List<ContestPhoto> getAllFilterAndSort(ContestPhotoFilterOptions contestPhotoFilterOptions) {
        try (Session session = sessionFactory.openSession()) {
            List<String> filters = new ArrayList<>();
            Map<String, Object> params = new HashMap<>();

            contestPhotoFilterOptions.getTitle().ifPresent(value -> {
                filters.add(" title like :title ");
                params.put("title", String.format("%%%s%%", value));
            });

            StringBuilder queryString = new StringBuilder("from ContestPhoto");
            if (!filters.isEmpty()) {
                queryString
                        .append(" where ")
                        .append(String.join(" and ", filters));
            }
            queryString.append(generateOrderBy(contestPhotoFilterOptions));

            Query<ContestPhoto> query = session.createQuery(queryString.toString(), ContestPhoto.class);
            query.setProperties(params);
            return query.list();
        }
    }

    @Override
    public ContestPhoto createAndReturnContestPhoto(ContestPhoto contestPhoto) {
        try (Session session = sessionFactory.openSession()) {
            session.beginTransaction();
            session.save(contestPhoto);
            session.getTransaction().commit();
        }
        return contestPhoto;
    }

    @Override
    public List<ContestPhoto> getContestPhotosByContestId(int contestId) {
        try (Session session = sessionFactory.openSession()) {
            Query<ContestPhoto> query = session.createQuery("SELECT cp FROM ContestPhoto cp " +
                    "JOIN cp.contest c WHERE c.id = :contestId", ContestPhoto.class);
            query.setParameter("contestId", contestId);

            return query.getResultList();
        }
    }

    @Override
    public List<WinnerPhoto> getAllWinners() {
        try (Session session = sessionFactory.openSession()) {
            Query<WinnerPhoto> query = session.createQuery("SELECT wp FROM WinnerPhoto wp", WinnerPhoto.class);

            return query.getResultList();
        }
    }

    @Override
    public List<ContestPhoto> getContestPhotosByUser(User user) {
        try (Session session = sessionFactory.openSession()) {
            Query<ContestPhoto> query = session.
                    createQuery("SELECT cp FROM ContestPhoto cp where cp.user = :user", ContestPhoto.class);

            query.setParameter("user", user);

            return query.getResultList();
        }
    }

    private String generateOrderBy(ContestPhotoFilterOptions contestPhotoFilterOptions) {
        String orderBy = " order by createdAt desc";

        if (contestPhotoFilterOptions.getSortBy().isEmpty()) {
            return orderBy;
        }

        orderBy = switch (contestPhotoFilterOptions.getSortBy().get()) {
            case "title" -> " title ";
            case "date" -> " createdAt ";
            default -> " ";
        };

        orderBy = String.format(" order by %s ", orderBy);

        if (contestPhotoFilterOptions.getSortOrder().isPresent() && contestPhotoFilterOptions.getSortOrder().get().equalsIgnoreCase("desc")) {
            orderBy = String.format(" %s desc ", orderBy);
        }

        return orderBy;

    }

}
