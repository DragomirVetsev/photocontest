package com.example.photocontest.repositories;

import com.example.photocontest.exceptions.EntityNotFoundException;
import com.example.photocontest.models.Rank;
import com.example.photocontest.repositories.contracts.RankRepository;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.query.Query;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public class RankRepositoryImpl implements RankRepository {

    private final SessionFactory sessionFactory;

    @Autowired
    public RankRepositoryImpl(SessionFactory sessionFactory) {
        this.sessionFactory = sessionFactory;
    }

    @Override
    public List<Rank> getAll() {
        try (Session session = sessionFactory.openSession()) {
            Query<Rank> query = session.createQuery("from Rank", Rank.class);
            return query.list();
        }
    }

    @Override
    public Rank get(int id) {
        try (Session session = sessionFactory.openSession()) {
            Rank rank = session.get(Rank.class, id);
            if (rank == null) {
                throw new EntityNotFoundException("Rank", id);
            }
            return rank;
        }
    }

    @Override
    public Rank get(String name) {
        try (Session session = sessionFactory.openSession()) {
            Query<Rank> query = session.createQuery("from Rank where name = :name", Rank.class);
            query.setParameter("name", name);

            List<Rank> result = query.list();
            if (result.size() == 0) {
                throw new EntityNotFoundException("Rank", "name", name);
            }
            return result.get(0);
        }
    }


    @Override
    public Rank getRankByScore(int score) {
        try (Session session = sessionFactory.openSession()) {
            Query<Rank> query = session
                    .createQuery("from Rank where minScore <= :score and maxScore >= :score", Rank.class);
            query.setParameter("score", score);

            List<Rank> result = query.list();
            if (result.size() == 0) {
                throw new EntityNotFoundException("Rank", "score", score);
            }
            return result.get(0);
        }
    }
}
