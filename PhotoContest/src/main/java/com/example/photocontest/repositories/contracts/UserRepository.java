package com.example.photocontest.repositories.contracts;

import com.example.photocontest.models.User;
import com.example.photocontest.models.UserFilterOptions;

import java.util.List;

public interface UserRepository {
    List<User> get(UserFilterOptions userFilterOptions);

    List<User> getAll();

    User get(int id);

    User get(String username);

    void create(User user);

    void update(User user);

    void delete(User user);

    List<User> getOrganizers();

    List<User> getUsersByRank(User user, String rankName);

    User findByEmail(String email);

    User getUserByUsername(String username);

    User findByResetPasswordToken(String token);

    void updateUserRangAndScoreInDB(User user, int newScore);

}
