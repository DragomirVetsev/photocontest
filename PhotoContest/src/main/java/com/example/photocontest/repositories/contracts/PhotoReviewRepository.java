package com.example.photocontest.repositories.contracts;

import com.example.photocontest.models.Contest;
import com.example.photocontest.models.ContestPhoto;
import com.example.photocontest.models.PhotoReview;
import com.example.photocontest.models.User;
import com.example.photocontest.models.dto.ContestPhotoAndPointsDto;

import java.util.List;

public interface PhotoReviewRepository {

    List<PhotoReview> getAllByContestPhoto(ContestPhoto contestPhotoId);

    PhotoReview get(int id, ContestPhoto contestPhoto);

    void create(PhotoReview photoReview);

    void update(PhotoReview photoReview);

    void delete(int id);

    List<ContestPhotoAndPointsDto> getDtosByContest(Contest contest);

    List<PhotoReview> getByContest(Contest contest);

    List<ContestPhoto> getAllByJuro(Contest contest, User juro);

    List<ContestPhoto> getBlockedPhotos(Contest contest);

    void deleteByContestPhotoId(int contestPhotoId);
}
