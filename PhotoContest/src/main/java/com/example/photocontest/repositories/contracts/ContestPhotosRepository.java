package com.example.photocontest.repositories.contracts;

import com.example.photocontest.models.ContestPhoto;
import com.example.photocontest.models.ContestPhotoFilterOptions;
import com.example.photocontest.models.User;
import com.example.photocontest.models.WinnerPhoto;

import java.util.List;

public interface ContestPhotosRepository extends BaseCRUDRepository<ContestPhoto> {
    List<ContestPhoto> getByUserAndContestIds(int userId, int contestId);

    List<ContestPhoto> getAllFilterAndSort(ContestPhotoFilterOptions contestPhotoFilterOptions);

    ContestPhoto createAndReturnContestPhoto(ContestPhoto contestPhoto);

    List<ContestPhoto> getContestPhotosByContestId(int id);

    List<WinnerPhoto> getAllWinners();

    List<ContestPhoto> getContestPhotosByUser(User user);
}
