package com.example.photocontest.repositories;

import com.example.photocontest.exceptions.EntityNotFoundException;
import com.example.photocontest.models.Contest;
import com.example.photocontest.models.ContestPhoto;
import com.example.photocontest.models.PhotoReview;
import com.example.photocontest.models.User;
import com.example.photocontest.models.dto.ContestPhotoAndPointsDto;
import com.example.photocontest.repositories.contracts.PhotoReviewRepository;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import org.hibernate.query.Query;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import java.util.ArrayList;
import java.util.List;

@Repository
public class PhotoReviewRepositoryImpl implements PhotoReviewRepository {

    private final SessionFactory sessionFactory;

    @Autowired
    public PhotoReviewRepositoryImpl(SessionFactory sessionFactory) {
        this.sessionFactory = sessionFactory;
    }


    @Override
    public List<PhotoReview> getAllByContestPhoto(ContestPhoto contestPhoto) {
        try (Session session = sessionFactory.openSession()) {
            return session.createQuery("from PhotoReview where contestPhoto = :contestPhoto", PhotoReview.class)
                    .setParameter("contestPhoto", contestPhoto)
                    .list();
        }
    }

    @Override
    public PhotoReview get(int id, ContestPhoto contestPhoto) {
        try (Session session = sessionFactory.openSession()) {
            Query<PhotoReview> query = session.
                    createQuery("from PhotoReview where id = :id and contestPhoto = :contestPhoto",
                            PhotoReview.class);
            query.setParameter("id", id);
            query.setParameter("contestPhoto", contestPhoto);

            List<PhotoReview> result = query.list();
            if (result.size() == 0) {
                throw new EntityNotFoundException("PhotoReview", id);
            }

            return result.get(0);
        }
    }

    @Override
    public void create(PhotoReview photoReview) {
        try (Session session = sessionFactory.openSession()) {
            session.beginTransaction();
            session.persist(photoReview);
            session.getTransaction().commit();
        }
    }

    @Override
    public void update(PhotoReview photoReview) {
        try (Session session = sessionFactory.openSession()) {
            session.beginTransaction();
            session.merge(photoReview);
            session.getTransaction().commit();
        }
    }

    @Override
    public void delete(int id) {
        PhotoReview photoReviewToDelete = get(id);
        try (Session session = sessionFactory.openSession()) {
            session.beginTransaction();
            session.remove(photoReviewToDelete);
            session.getTransaction().commit();
        }
    }

    @Override
    public List<ContestPhotoAndPointsDto> getDtosByContest(Contest contest) {
        try (Session session = sessionFactory.openSession()) {
            Query<Object[]> query = session.
                    createQuery("SELECT pr.contestPhoto, AVG(pr.score) FROM PhotoReview pr " +
                            "WHERE pr.contest = :contest GROUP BY pr.contestPhoto ORDER BY AVG(pr.score) DESC", Object[].class);
            query.setParameter("contest", contest);

            List<Object[]> result = query.list();
            if (result.size() == 0) {
                throw new EntityNotFoundException("No photo reviews about contest with ID: " + contest.getId());
            }

            List<ContestPhotoAndPointsDto> dtoList = new ArrayList<>();
            for (Object[] row : result) {
                ContestPhoto contestPhoto = (ContestPhoto) row[0];
                double score = ((Number) row[1]).intValue();
                ContestPhotoAndPointsDto dto = new ContestPhotoAndPointsDto(contestPhoto, score);
                dtoList.add(dto);
            }
            return dtoList;
        }
    }

    @Override
    public List<PhotoReview> getByContest(Contest contest) {
        try (Session session = sessionFactory.openSession()) {
            Query<PhotoReview> query = session.createQuery("SELECT pr " +
                    "FROM PhotoReview pr " +
                    "WHERE pr.contest = :contest", PhotoReview.class);
            query.setParameter("contest", contest);

            return query.list();
        }
    }

    @Override
    public List<ContestPhoto> getAllByJuro(Contest contest, User juro) {
        try (Session session = sessionFactory.openSession()) {
            Query<ContestPhoto> query = session.createQuery("SELECT pr.contestPhoto " +
                    "FROM PhotoReview pr " +
                    "WHERE pr.contest = :contest and pr.juryMember =:juro", ContestPhoto.class);
            query.setParameter("contest", contest);
            query.setParameter("juro", juro);

            return query.list();
        }
    }

    @Override
    public List<ContestPhoto> getBlockedPhotos(Contest contest) {
        try (Session session = sessionFactory.openSession()) {
            Query<ContestPhoto> query = session.createQuery("SELECT pr.contestPhoto " +
                    "FROM PhotoReview pr " +
                    "WHERE pr.contest = :contest AND pr.isWrongCategory = true ", ContestPhoto.class);
            query.setParameter("contest", contest);

            return query.list();
        }
    }

    @Override
    public void deleteByContestPhotoId(int contestPhotoId) {
        try (Session session = sessionFactory.openSession()) {
            Transaction transaction = session.beginTransaction();
            Query<PhotoReview> query = session.createQuery("DELETE FROM PhotoReview pr " +
                    "WHERE pr.contestPhoto.id = :contestPhotoId");
            query.setParameter("contestPhotoId", contestPhotoId);
            int result = query.executeUpdate();
            transaction.commit();
            System.out.println(result + " rows deleted.");
        }

    }

    private PhotoReview get(int id) {
        try (Session session = sessionFactory.openSession()) {
            PhotoReview photoReview = session.get(PhotoReview.class, id);
            if (photoReview == null) {
                throw new EntityNotFoundException("PhotoReview", id);
            }
            return photoReview;
        }
    }
}
