package com.example.photocontest.repositories;

import com.example.photocontest.models.Place;
import com.example.photocontest.repositories.contracts.PlaceRepository;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

@Repository
public class PlaceRepositoryImpl extends AbstractCRUDRepository<Place> implements PlaceRepository {
    SessionFactory sessionFactory;

    @Autowired
    public PlaceRepositoryImpl(SessionFactory sessionFactory) {
        super(Place.class, sessionFactory);
        this.sessionFactory = sessionFactory;
    }
}
