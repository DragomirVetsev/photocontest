package com.example.photocontest.repositories.contracts;


import com.example.photocontest.models.Contest;
import com.example.photocontest.models.ContestFilterOptions;

import java.time.LocalDateTime;
import java.util.List;

public interface ContestRepository extends BaseCRUDRepository<Contest> {


    List<Contest> get(ContestFilterOptions contestFilterOptions);

    List<Contest> getContestPhaseOne(LocalDateTime currentDate);

    List<Contest> getContestPhaseTwo(LocalDateTime currentDate);

    List<Contest> getContestPhaseFinished(LocalDateTime currentDate);

    Contest getById(int contestPhotoId);

    long getNumberOfContests();

    List<Contest> getMoreRecentContests();

    List<Contest> getFinishedContestsWithoutWinners();
}
