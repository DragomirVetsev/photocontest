package com.example.photocontest.repositories.contracts;

import com.example.photocontest.models.Place;

public interface PlaceRepository extends BaseCRUDRepository<Place> {
}
