package com.example.photocontest.services;

import com.example.photocontest.Helpers;
import com.example.photocontest.exceptions.DuplicateEntityException;
import com.example.photocontest.exceptions.EntityNotFoundException;
import com.example.photocontest.exceptions.UnauthorizedOperationException;
import com.example.photocontest.models.Category;
import com.example.photocontest.models.User;
import com.example.photocontest.repositories.contracts.CategoryRepository;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.jupiter.MockitoExtension;

import java.util.ArrayList;
import java.util.List;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.Mockito.when;

@ExtendWith(MockitoExtension.class)
class CategoryServiceImplTests {

    @Mock
    CategoryRepository mockRepository;
    @InjectMocks
    CategoryServiceImpl categoryService;

    @Test
    void getById_Should_ReturnCategory_When_MatchByIdExist() {
        // Arrange
        Category mockCategory = Helpers.createMockCategory();

        when(mockRepository.getById(Mockito.anyInt()))
                .thenReturn(mockCategory);
        // Act
        Category result = categoryService.getById(mockCategory.getId());
        // Assert
        assertEquals(mockCategory, result);
    }

    @Test
    void GetByCategoryNameShouldReturnCategoryWhenMatchByNameExist() {
        // Arrange
        Category mockCategory = Helpers.createMockCategory();
        when(mockRepository.getByField("name", mockCategory.getName())).thenReturn(mockCategory);

        // Act
        Category result = categoryService.getByCategoryName(mockCategory.getName());
        // Assert
        assertEquals(mockCategory, result);
    }

    @Test
    void CategoryExistShouldReturnTrueIfCategoryExist() {
        // Arrange
        List<Category> categories = new ArrayList<>();
        categories.add(Helpers.createMockCategory());
        categories.add(Helpers.createMockOtherCategory());
        when(mockRepository.getAll()).thenReturn(categories);


        // Act and assert
        boolean result1 = categoryService.categoryExist(Helpers.MOCK_CATEGORY_NAME);
        assertTrue(result1);

        boolean result2 = categoryService.categoryExist("Non-existing Category");
        assertFalse(result2);
    }

    @Test
    void create_Should_Throw_When_CategoryWithSameTitleExist() {
        // Arrange and act
        Category mockCategory = Helpers.createMockCategory();
        User mockUser = Helpers.createMockOrganizer();
        Category mockOtherCategory = Helpers.createMockOtherCategory();
        mockOtherCategory.setName(mockCategory.getName());
        when(mockRepository.getByField("name", mockCategory.getName())).thenReturn(mockOtherCategory);


        // Assert
        assertThrows(DuplicateEntityException.class, () -> {
            categoryService.create(mockCategory, mockUser);
        });
    }

    @Test
    void create_Should_Throw_When_JunkieWantToCreate() {
        // Arrange and act
        Category mockCategory = Helpers.createMockCategory();
        User mockUser = Helpers.createMockPhotoJunkie();

        // Assert
        assertThrows(UnauthorizedOperationException.class, () -> {
            categoryService.create(mockCategory, mockUser);
        });
    }

    @Test
    void create_Should_CreateCategory_If_NameIsUniqueAndOrganizerWantToCreate() {
        // Arrange and act

        Category mockCategory = Helpers.createMockCategory();
        User organizer = Helpers.createMockOrganizer();
        Mockito.when(mockRepository.getByField("name", mockCategory.getName()))
                .thenThrow(EntityNotFoundException.class);

        // Act
        categoryService.create(mockCategory, organizer);

        // Assert
        Mockito.verify(mockRepository, Mockito.times(1))
                .create(mockCategory);
    }

    @Test
    void getAll_Should_CallRepository() {
        // Arrange
        Mockito.when(mockRepository.getAll())
                .thenReturn(List.of());
        // Act
        categoryService.getAll();
        // Assert
        Mockito.verify(mockRepository, Mockito.times(1))
                .getAll();
    }

    @Test
    void delete_Should_DeleteCategory_If_OrganizerWantToDelete() {
        // Arrange and act

        Category mockCategory = Helpers.createMockCategory();
        User organizer = Helpers.createMockOrganizer();
        // Act
        categoryService.delete(mockCategory.getId(), organizer);

        // Assert
        Mockito.verify(mockRepository, Mockito.times(1))
                .delete(mockCategory.getId());
    }

    @Test
    void delete_Should_Throw_When_JunkieWantToDelete() {
        // Arrange
        Category mockCategory = Helpers.createMockCategory();
        User mockUser = Helpers.createMockPhotoJunkie();

        // Act and Assert
        assertThrows(UnauthorizedOperationException.class, () -> {
            categoryService.delete(mockCategory.getId(), mockUser);
        });
    }

    @Test
    void update_Should_UpdateCategory_If_NameIsUniqueAndOrganizerWantToUpdate() {
        // Arrange and act

        Category mockCategory = Helpers.createMockCategory();
        User organizer = Helpers.createMockOrganizer();
        Mockito.when(mockRepository.getByField("name", mockCategory.getName()))
                .thenThrow(EntityNotFoundException.class);

        // Act
        categoryService.update(mockCategory, organizer);

        // Assert
        Mockito.verify(mockRepository, Mockito.times(1))
                .update(mockCategory);
    }

    @Test
    void update_Should_Throw_When_NameIsUniqueAndJunkieWantToUpdate() {
        // Arrange and act

        Category mockCategory = Helpers.createMockCategory();
        User photoJunkie = Helpers.createMockPhotoJunkie();

        // Assert
        assertThrows(UnauthorizedOperationException.class, () -> {
            categoryService.update(mockCategory, photoJunkie);
        });
    }

    @Test
    void update_Should_Throw_When_NameIsDuplicate() {
        // Arrange and act

        Category mockCategory = Helpers.createMockCategory();
        Category otherCategory = Helpers.createMockOtherCategory();
        otherCategory.setName(mockCategory.getName());
        User organizer = Helpers.createMockOrganizer();

        Mockito.when(mockRepository.getByField("name", mockCategory.getName()))
                .thenReturn(otherCategory);

        // Assert
        assertThrows(DuplicateEntityException.class, () -> {
            categoryService.update(mockCategory, organizer);
        });
    }


}



