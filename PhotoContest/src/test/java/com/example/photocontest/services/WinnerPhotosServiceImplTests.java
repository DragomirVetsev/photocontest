package com.example.photocontest.services;

import com.example.photocontest.exceptions.EntityNotFoundException;
import com.example.photocontest.exceptions.InvalidTimePeriodException;
import com.example.photocontest.models.Contest;
import com.example.photocontest.models.ContestPhoto;
import com.example.photocontest.models.WinnerPhoto;
import com.example.photocontest.repositories.contracts.*;
import com.example.photocontest.services.contracts.ImageFilesOperations;
import jakarta.persistence.EntityExistsException;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.jupiter.MockitoExtension;

import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;

import static com.example.photocontest.Helpers.*;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;

@ExtendWith(MockitoExtension.class)
public class WinnerPhotosServiceImplTests {
    @Mock
    private ContestPhotosRepository mockContestPhotosRepository;
    @Mock
    private WinnerPhotosRepository mockWinnerPhotosRepository;
    @Mock
    private UserRepository mockUserRepository;
    @Mock
    private PhotoReviewRepository mockPhotoReviewRepository;
    @Mock
    private ImageFilesOperations mockImageFileOperations;
    @Mock
    private ContestRepository mockContestRepository;
    @Mock
    private PlaceRepository mockPlaceRepository;
    @Mock
    private WinnerPhotosServiceImpl mockWinnerPhotosService;


    @InjectMocks
    private WinnerPhotosServiceImpl winnerPhotosService;

    @Test
    public void testGetAllWinners() {
        // Arrange
        Contest contest = createMockContestNotInvitationalFinished();
        contest.setId(1);

        // Assert
        Mockito.when(mockContestRepository.getById(1))
                .thenReturn(contest);
        // Act
        Assertions.assertThrows(EntityNotFoundException.class, () -> winnerPhotosService.createWinners(contest.getId()));
    }

    @Test
    public void testCreateWinnersThrowsExceptionWhenPhaseTwoNotFinished() {
        // Prepare test data
        Contest contest = createMockContestNotInvitational();
        contest.setPhaseTwoEnd(LocalDateTime.now().plusDays(1));

        // Set up mock behavior
        Mockito.when(mockContestRepository.getById(contest.getId()))
                .thenReturn(contest);

        // Call the method and assert that it throws the expected exception
        assertThrows(InvalidTimePeriodException.class, () -> {
            winnerPhotosService.createWinners(contest.getId());
        });
    }

    @Test
    public void testCreateWinners_ThrowsException_WhenWinnerAlreadyCreated() {
        // Prepare test data
        Contest contest = createMockContestNotInvitational();
        contest.setPhaseTwoEnd(LocalDateTime.now().minusDays(1));

        ArrayList<ContestPhoto> someContestPhotos = new ArrayList<>();
        someContestPhotos.add(createMockContestPhoto());
        // Set up mock behavior
        Mockito.when(mockContestRepository.getById(contest.getId()))
                .thenReturn(contest);
        Mockito.when(mockWinnerPhotosRepository.getContestPhotosWinnersByContestId(contest.getId()))
                .thenReturn(someContestPhotos);

        // Call the method and assert that it throws the expected exception
        assertThrows(EntityExistsException.class, () -> {
            winnerPhotosService.createWinners(contest.getId());
        });
    }

    @Test
    public void testGetWinnerPhotos_ByContest_WithWinners() {
        // Prepare some test data
        Contest contest = createMockContestNotInvitational();
        List<WinnerPhoto> winnerPhotos = new ArrayList<>();
        WinnerPhoto winnerPhoto = createMockWinnerPhoto();
        winnerPhotos.add(winnerPhoto);

        // Set up the mock behavior
        Mockito.when(mockWinnerPhotosRepository.getWinnerPhotosByContest(contest))
                .thenReturn(winnerPhotos);

        // Create the service and call the method
        List<WinnerPhoto> result = winnerPhotosService.getWinnerPhotosByContest(contest);

        // Verify the results
        Mockito.verify(mockWinnerPhotosRepository).getWinnerPhotosByContest(contest);
        assertEquals(winnerPhotos, result);
    }

    @Test
    public void testГivePointsToUnreviewedContestPhotos_ThrowsExceptionWhen_PhaseTwoNotFinished() {
        // Prepare test data
        Contest contest = createMockContestNotInvitational();
        contest.setPhaseTwoEnd(LocalDateTime.now().plusDays(1));

        // Set up mock behavior
        Mockito.when(mockContestRepository.getById(contest.getId()))
                .thenReturn(contest);

        // Call the method and assert that it throws the expected exception
        assertThrows(InvalidTimePeriodException.class, () -> {
            winnerPhotosService.givePointsToUnreviewedContestPhotos(contest);
        });
    }
}
