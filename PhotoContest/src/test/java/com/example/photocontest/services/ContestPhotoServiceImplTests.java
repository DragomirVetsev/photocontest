package com.example.photocontest.services;

import com.example.photocontest.exceptions.InvalidTimePeriodException;
import com.example.photocontest.exceptions.UnauthorizedOperationException;
import com.example.photocontest.models.*;
import com.example.photocontest.repositories.contracts.*;
import com.example.photocontest.services.contracts.ImageFilesOperations;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.web.multipart.MultipartFile;

import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import static com.example.photocontest.Helpers.*;


@ExtendWith(MockitoExtension.class)
public class ContestPhotoServiceImplTests {
    @Mock
    private ContestPhotosRepository mockContestPhotosRepository;
    @Mock
    private WinnerPhotosRepository mockWinnerPhotosRepository;
    @Mock
    private UserRepository mockUserRepository;
    @Mock
    private PhotoReviewRepository mockPhotoReviewRepository;
    @Mock
    private ImageFilesOperations mockImageFileOperations;
    @Mock
    private ContestRepository mockContestRepository;


    @InjectMocks
    private ContestPhotoServiceImpl contestPhotoService;

    @Test
    public void getAll_Should_CallRepository() {
        // Arrange
        ContestPhotoFilterOptions filterOptions = new ContestPhotoFilterOptions();
        Mockito.when(mockContestPhotosRepository.getAllFilterAndSort(filterOptions))
                .thenReturn(new ArrayList<>());

        // Act
        contestPhotoService.getAll(filterOptions);

        // Assert
        Mockito.verify(mockContestPhotosRepository,
                Mockito.times(1)).getAllFilterAndSort(filterOptions);
    }

    @Test
    public void getAll_Should_ReturnListOfContestPhotos() {
        // Arrange
        ContestPhotoFilterOptions filterOptions = new ContestPhotoFilterOptions();
        List<ContestPhoto> expectedList = List.of(
                createMockContestPhoto()
        );
        Mockito.when(mockContestPhotosRepository.getAllFilterAndSort(filterOptions))
                .thenReturn(expectedList);

        // Act
        List<ContestPhoto> actualList = contestPhotoService.getAll(filterOptions);

        // Assert
        Assertions.assertEquals(expectedList, actualList);
    }

    @Test
    public void get_Should_ReturnContestPhoto() {
        // Arrange
        int id = 1;
        ContestPhoto expectedContestPhoto = createMockContestPhoto();
        Mockito.when(mockContestPhotosRepository.getById(id))
                .thenReturn(expectedContestPhoto);

        // Act
        ContestPhoto actualContestPhoto = contestPhotoService.get(id);

        // Assert
        Assertions.assertEquals(expectedContestPhoto, actualContestPhoto);
        Mockito.verify(mockContestPhotosRepository, Mockito.times(1)).getById(id);
    }

    @Test
    public void createContestPhotoObj_Should_ReturnContestPhoto_When_ValidInput() {
        // Arrange
        ContestPhoto contestPhotoToCreate = createMockContestPhoto();
        contestPhotoToCreate.setUser(createMockPhotoJunkie());
        contestPhotoToCreate.setContest(createMockContestNotInvitational());
        contestPhotoToCreate.setPhotoUrl("test.jpg");

        ContestPhoto createdContestPhoto = createMockContestPhoto();

        Mockito.when(mockContestPhotosRepository.createAndReturnContestPhoto(Mockito.any(ContestPhoto.class)))
                .thenReturn(createdContestPhoto);

        // Act
        ContestPhoto result = contestPhotoService.createContestPhotoObj(contestPhotoToCreate);

        // Assert
        Assertions.assertNotNull(result);
        Assertions.assertEquals(createdContestPhoto.getId(), result.getId());
        Assertions.assertEquals(createdContestPhoto.getUser(), result.getUser());
        Assertions.assertEquals(createdContestPhoto.getContest(), result.getContest());
        Assertions.assertEquals(createdContestPhoto.getPhotoUrl(), result.getPhotoUrl());
        Assertions.assertNotNull(result.getCreatedAt());

        Mockito.verify(mockContestPhotosRepository,
                Mockito.times(1)).createAndReturnContestPhoto(Mockito.any(ContestPhoto.class));
    }

    @Test
    public void testDelete_ContestEnded_ThrowsInvalidTimePeriodException() {
        // Arrange
        int contestPhotoId = 1;
        User user = createMockOrganizer();
        ContestPhoto contestPhoto = createMockContestPhoto();
        Contest contest = createMockContestNotInvitational();
        contest.setPhaseTwoEnd(LocalDateTime.now().minusDays(1));
        contestPhoto.setContest(contest);

        Mockito.when(mockContestPhotosRepository.getById(Mockito.eq(contestPhotoId))).thenReturn(contestPhoto);

        // Act and Assert
        Assertions.assertThrows(InvalidTimePeriodException.class, () -> contestPhotoService.delete(contestPhotoId, user));
    }

    @Test
    public void testDelete_UserNotJuryOrOrganizer_ThrowsUnauthorizedOperationException() {
        // Arrange
        int contestPhotoId = 1;
        User user = createMockPhotoJunkie();
        ContestPhoto contestPhoto = createMockContestPhoto();
        Contest contest = createMockContestNotInvitational();
        contestPhoto.setContest(contest);

        Mockito.when(mockContestPhotosRepository.getById(Mockito.eq(contestPhotoId))).thenReturn(contestPhoto);

        // Act and Assert
        Assertions.assertThrows(UnauthorizedOperationException.class, () -> contestPhotoService.delete(contestPhotoId, user));
    }

    @Test
    public void testDelete_OrganizerDeletesPhoto_SuccessfulDelete() {
        // Arrange
        int contestPhotoId = 1;
        User user = createMockOrganizer();
        ContestPhoto contestPhoto = createMockContestPhoto();
        Contest contest = createMockContestNotInvitational();
        contestPhoto.setContest(contest);

        Mockito.when(mockContestPhotosRepository.getById(Mockito.eq(contestPhotoId))).thenReturn(contestPhoto);

        // Act
        contestPhotoService.delete(contestPhotoId, user);

        // Assert
        Mockito.verify(mockContestPhotosRepository, Mockito.times(1)).delete(Mockito.eq(contestPhotoId));
        Mockito.verify(mockImageFileOperations, Mockito.times(1)).deleteImage(Mockito.eq(contestPhoto.getPhotoUrl()));
        Mockito.verify(mockPhotoReviewRepository, Mockito.times(1)).deleteByContestPhotoId(Mockito.eq(contestPhotoId));
        Mockito.verify(mockUserRepository, Mockito.times(1)).updateUserRangAndScoreInDB(Mockito.eq(user), Mockito.anyInt());
    }

    @Test
    public void testCreateContestPhoto_SuccessfulCreationAndImageUpload() throws Exception {
        // Arrange
        ContestPhoto contestPhoto = createMockContestPhoto();
        MultipartFile mockFile = Mockito.mock(MultipartFile.class);

        // Act
        contestPhotoService.createContestPhoto(contestPhoto, mockFile);

        // Assert
        Mockito.verify(mockContestPhotosRepository, Mockito.times(1)).createAndReturnContestPhoto(Mockito.eq(contestPhoto));
        Mockito.verify(mockImageFileOperations, Mockito.times(1)).uploadImage(Mockito.eq(mockFile), Mockito.eq(contestPhoto.getPhotoUrl()));
    }

    @Test
    public void testGetAllWinners_ReturnsListOfWinnerPhotos() {
        // Arrange
        List<WinnerPhoto> winners = new ArrayList<>();
        winners.add(createMockWinnerPhoto());

        Mockito.when(mockContestPhotosRepository.getAllWinners()).thenReturn(winners);

        // Act
        List<WinnerPhoto> result = contestPhotoService.getAllWinners();

        // Assert
        Assertions.assertEquals(winners, result);
    }

    @Test
    public void testGetAllWinners_ReturnsEmptyListIfNoWinners() {
        // Arrange
        Mockito.when(mockContestPhotosRepository.getAllWinners()).thenReturn(new ArrayList<>());

        // Act
        List<WinnerPhoto> result = contestPhotoService.getAllWinners();

        // Assert
        Assertions.assertEquals(new ArrayList<>(), result);
    }

    @Test
    public void testSetUserPointsForParticipation_NonInvitationalContest_UserPointsUpdated() {
        // Arrange
        User user = createMockJurorPhotoJunkie();
        Contest contest = createMockContestNotInvitational();

        Mockito.when(mockContestRepository.getById(Mockito.eq(contest.getId()))).thenReturn(contest);

        // Act
        contestPhotoService.setUserPointsForParticipation(user, contest.getId());

        // Assert
        Mockito.verify(mockUserRepository, Mockito.times(1))
                .updateUserRangAndScoreInDB(Mockito.eq(user), Mockito.eq(user.getScore() + 1));
    }

    @Test
    public void testSetUserPointsForParticipation_InvitationalContest_UserPointsUpdated() {
        // Arrange
        User user = createMockPhotoJunkie();
        Contest contest = createMockContestInvitational();

        Mockito.when(mockContestRepository.getById(Mockito.eq(contest.getId()))).thenReturn(contest);

        // Act
        contestPhotoService.setUserPointsForParticipation(user, contest.getId());

        // Assert
        Mockito.verify(mockUserRepository, Mockito.times(1))
                .updateUserRangAndScoreInDB(Mockito.eq(user), Mockito.eq(user.getScore() + 3));
    }

}
