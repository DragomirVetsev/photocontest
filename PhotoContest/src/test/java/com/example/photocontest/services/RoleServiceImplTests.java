package com.example.photocontest.services;

import com.example.photocontest.repositories.contracts.RoleRepository;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.jupiter.MockitoExtension;

@ExtendWith(MockitoExtension.class)
class RoleServiceImplTests {

    @Mock
    RoleRepository mockRepository;

    @InjectMocks
    RoleServiceImpl roleService;

    @Test
    void getAll_Should_CallRepository() {
        // Act
        roleService.getAll();

        // Assert
        Mockito.verify(mockRepository, Mockito.times(1))
                .getAll();
    }


    @Test
    void get_Should_CallRepository_When_RankIdExists() {
        // Arrange
        int mockRoleId = 1;
        Mockito.when(mockRepository.get(mockRoleId))
                .thenReturn(null);

        // Act
        roleService.get(mockRoleId);

        // Assert
        Mockito.verify(mockRepository, Mockito.times(1))
                .get(mockRoleId);
    }


    @Test
    void get_Should_CallRepository_When_RankNameExists() {
        // Arrange
        String mockRoleName = "Admin";
        Mockito.when(mockRepository.get(mockRoleName))
                .thenReturn(null);

        // Act
        roleService.get(mockRoleName);

        // Assert
        Mockito.verify(mockRepository, Mockito.times(1))
                .get(mockRoleName);
    }

}
